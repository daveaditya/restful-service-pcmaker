<?php

/** Adds the file containing autoload instructions */
require getcwd() . "/vendor/autoload.php";

/** Alias for ServerRequestInterface */
use Psr\Http\Message\ServerRequestInterface as Request;

/** Alias for ResponseInterface */
use Psr\Http\Message\ResponseInterface as Response;

/** Alias for \Slim\App */
use Slim\App as SlimApp;

/** Alias for the Controllers */
use PCMaker\Controllers\{
    AccountController, AuthController, PCPartsController
};

/** Alias for the FeedbackMessage class */
use PCMaker\Helpers\FeedbackMessage;


/**
 * Configurations for \Slim\App to enable debugging.
 * For development purpose only.
 */
$config["settings"] = array("displayErrorDetails"=>true, "debug"=>true, "addContentLengthHeader"=>false);

/** @var $app \Slim\App */
$app = new SlimApp($config);

// Enable display of error in PHP
error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');


try {

    // Welcome the client to PC Maker's API
    $app->get("/", function (Request $request, Response $response) {

        // A creates a new FeedbackMessage object
        $feedback = new FeedbackMessage();

        // Sets the current_url of the FeedbackMessage
        $feedback->setCurrentLink($request->getUri());

        // Create an welcome message Associative array
        $welcome = array("feedback"=>$feedback ,"data"=>array("Message" => "Welcome to PC Maker and Services"));

        // Create a JSON string from the array and return to the client
        /** @noinspection PhpUndefinedMethodInspection */
        return $response->withJson($welcome, 200, JSON_NUMERIC_CHECK|JSON_UNESCAPED_SLASHES);

    });


    $app->group("/{table_name: technicians|organizations}", function () {

        // Handles the request to get multiple account details
        $this->get("", AccountController::class . ":requestForMultipleAccounts");

        // Handles the request for login of organization and technicians
        $this->post("/login", AuthController::class . ":requestToLogin");

        // Handles the request to signup from organization and technician
        $this->post("/signup", AuthController::class . ":requestToSignUp");

        // Handles the request to reset password from organization and technician
        $this->put("/reset", AuthController::class . ":requestToResetPassword");

        // Handles the update request to delete an account
        $this->delete("/delete", AccountController::class . ":requestToDeleteAccount");

        // Handles a request for account details, which is provided when logged in
        $this->get("/{username}", AccountController::class . ":requestForAccountDetails");

        // Handles the request to create a new account
        $this->put("/{username}", AccountController::class . ":requestToUpdateAccountDetails");

        // Handles the request to add new stock
        $this->post("/{username}/stocks", AccountController::class . ":requestToAddStock");

        // Handles the request to get the current stock data
        $this->get("/{username}/stocks", AccountController::class . ":requestToGetMultipleStock");

        // Handles the request to get the current stock data
        $this->get("/{username}/stocks/{id}", AccountController::class . ":requestToGetStock");

        // Handles the request to update stock
        $this->put("/{username}/stocks/{id}", AccountController::class . ":requestToUpdateStock");

        // Handles the request to delete a stock entry
        $this->delete("/{username}/stocks/{id}", AccountController::class . ":requestToDeleteStock");

        // Handles a request for registered technicians under the organization
        $this->post("/{username}/new", AccountController::class . ":requestToAddNewArrival");

        // Handles a request for registered technicians under the organization
        $this->get("/{username}/new", AccountController::class . ":requestToGetNewArrivals");

        // Handles a request for registered technicians under the organization
        $this->delete("/{username}/new/{id}", AccountController::class . ":requestToDeleteNewArrival");

    });


    $app->group("/{table_name: organizations}", function() {

        // Handles a request to get registered technicians under the organization
        $this->get("/{username}/technicians", AccountController::class . ":requestForRegisteredTechnicians");

        // Handles a request to get a single registered technician under the organization
        $this->get("/{username}/technicians/{id}", AccountController::class . ":requestForSingleRegisteredTechnician");

        // Handles a request to add new technicians under the organization
        $this->post("/{username}/technicians", AccountController::class . ":requestToAddTechnician");

        // Handles a request to delete the technician under the organization
        $this->post("/{org_username}/technicians/{tech_username}", AccountController::class . ":requestToRemoveTechnician");

    });


    // Handles the request to add a new tuple
    $app->post("/{table_name: [a-z_]+s}", PCPartsController::class . ":requestToInsertItem");


    // Handles a single tuple request
    $app->get("/{table_name: [a-z_]+s}/{id}", PCPartsController::class . ":requestForSingleItem");


    // Handles the request to get multiple data tuple
    $app->get("/{table_name: [a-z_]+s}", PCPartsController::class . ":requestForMultipleItems");


    // Handles the update request for a tuple
    $app->put("/{table_name: [a-z_]+s}/{id}", PCPartsController::class . ":requestToUpdateItem");


    // Handles the update request for a tuple
    $app->delete("/{table_name: [a-z_]+s}/{id}", PCPartsController::class . ":requestToDeleteItem");


} catch (RuntimeException $exception) {
    var_dump($exception);
}


// Run the configured Slim App
$app->run();