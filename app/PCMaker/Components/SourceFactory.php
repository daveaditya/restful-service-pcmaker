<?php

/** Namespace for the SourceFactory class */
namespace PCMaker\Components;

/** Alias for the InvalidRequest Exception class */
use PCMaker\Exceptions\InvalidRequest;

/** Alias for the Constants class */
use PCMaker\Helpers\Constants;


/**
 * Class DataSourceFactory
 * A DataSource factory that creates a DataSource object based on the given data source name
 * @package PCMaker\Components
 */
class SourceFactory {

    /**
     * A method that returns a AccountSource object based on the requested table
     * @param string $request Name of the requested Data Source / Table
     * @return AccountSource A AccountSource object connected to the requested data source
     * @throws InvalidRequest If the requested data source does not exists throw InvalidRequest
     */
    public static function getAccountInstance(string $request): AccountSource {

        // Checks whether the requested data source is valid or not
        if (array_key_exists($request, Constants::ORG_TABLES)) {
            // If valid get the details related to the data source
            $details = Constants::ORG_TABLES[$request];

            // And return a new DataSource object with the obtained details as parameters
            return new AccountSource($details);

        // Else throw the InvalidRequest exception, with the request_table as argument
        } else {
            throw new InvalidRequest($request);
        }

    }


    /**
     * A method that returns a PCPartsSource object based on the requested table
     * @param string $request Name of the requested Data Source
     * @return PCPartsSource A PCPartsSource object connected to the requested data source
     * @throws InvalidRequest If the requested data source does not exists throw InvalidRequest
     */
    public static function getPCPartsInstance(string $request): PCPartsSource {

        // Checks whether the requested data source is valid or not
        if (array_key_exists($request, Constants::HARDWARE_RELATED_TABLES)) {
            // If valid get the details related to the data source
            $details = Constants::HARDWARE_RELATED_TABLES[$request];

            // And return a new DataSource object with the obtained details as parameters
            return new PCPartsSource($details);

        // Else throw the InvalidRequest exception, with the request_table as argument
        } else {
            throw new InvalidRequest($request);
        }

    }
}