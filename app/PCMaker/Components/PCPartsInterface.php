<?php

/** Namespace for the PCPartsInterface */
namespace PCMaker\Components;

/** Alias for the custom Exceptions */
use PCMaker\Exceptions\{
    DeletionFailed, EmptyResultSet, InsertionFailed, InvalidArgument, InvalidFieldsParameter, InvalidID, InvalidOrder, UpdateFailed
};


/**
 * Interface PCPartsInterface
 * An interface that specifies the methods that are to be declared by the PCPartsSource class
 * @package PCMaker\Components
 */
interface PCPartsInterface {

    /**
     * A method that inserts the given values in the Data Source / Table
     * @param array $fields_fields_values The fields and values that are valid for updating
     * @return array An array containing the final result of insertion
     * @throws InsertionFailed If there is some problem with the insert operation throw InsertionFailed exception
     * @throws InvalidArgument If the fields specified are not valid throw InvalidArgument exception
     */
    function insertItem(array $fields_fields_values): array;


    /**
     * A method that returns multiple objects from the queried DataSource
     * @param array|null $fields Value of the 'fields' argument in form of array, that must be returned
     * @param array|null $sort_by $fields Value of the 'sort_by' argument in form of array, that is used for sorting
     * @param string $order Specifies whether to sort ascending ASC (default) or descending DESC
     * @param int $from Offset in the DataSource from which data is returned
     * @param int $per_page Number of tuple to return
     * @param string $response_group Specifies the attributes to return for an item
     * @return array An array of tuple containing data as per the request
     * @throws EmptyResultSet If there is no tuple with the given ID throw EmptyResultSet exception
     * @throws InvalidOrder If the value of order parameter is incorrect throw InvalidOrder exception
     */
    function getMultipleItems(array $fields = null, array $sort_by = null, $order = "ASC", int $from = 0,
                              int $per_page = 0, string $response_group = "info"): array ;


    /**
     * A method that returns a single object from the queried DataSource
     * @param string $id ID of the tuple to return
     * @param array|null $fields Value of the 'fields' argument in form of array, that must be returned
     * @param string $response_group Specifies the attributes to return for an item
     * @return array An array containing a single tuple
     * @throws InvalidFieldsParameter If the fields specified are not correct throw InvalidFieldsParameter
     * @throws EmptyResultSet If there is no tuple with the given ID throw EmptyResultSet
     */
    function getSingleItem(string $id = null, array $fields = null, string $response_group = "info"): array;


    /**
     * A method that updates the tuple with given ID
     * @param string $id ID of the tuple that needs to be updated
     * @param array $fields_values The fields and values that are valid for updating
     * @return array An array containing the final result of updating
     * @throws InvalidArgument If the fields specified are not valid throw InvalidArgument exception
     * @throws UpdateFailed If the update operation fails throw UpdateFailed exception
     */
    function updateItem(string $id, array $fields_values): array;


    /**
     * A method that deletes the tuple with given id
     * @param string $id ID of the tuple which needs to be deleted
     * @return array An array containing the final result of deletion
     * @throws DeletionFailed If the delete operation fails throw DeletionFailed exception
     * @throws InvalidID If the entered ID is not valid throw InvalidID exception
     */
    function deleteItem(string $id): array;

}