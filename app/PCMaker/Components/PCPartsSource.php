<?php

/** Define namespace for the DataSource class */
namespace PCMaker\Components;

/** Alias for the Constants class containing various constants */
use PCMaker\Helpers\{
    Constants, FeedbackMessage
};

/** Alias for the custom Exceptions */
use PCMaker\Exceptions\{
    DeletionFailed, EmptyResultSet, InsertionFailed, InvalidArgument, InvalidFieldsParameter, InvalidID, InvalidOrder, InvalidResponseGroup, ResponseGroupNotSupported, UpdateFailed
};

/** Brings the PDO to current namespace */
use PDO;


/**
 * Class PCPartsSource
 * This class deals with the requests data tuple
 * @package PCMaker\DataSource
 */
class PCPartsSource extends DataSource implements PCPartsInterface {

    /**
     * Checks whether the value of the response_group parameter is valid or not
     * @param string $response_group The response_group value given in the request url
     * @throws InvalidResponseGroup If the response_group value if invalid
     */
    private static function sanitizeResponseGroup(string $response_group) {
        // Gets an array of all the response groups from the request's parameters
        $input_response_group = explode(",", $response_group);

        // If the input response group contains items that are not in the valid_response_group
        // Return false
        if (count(array_diff($input_response_group, Constants::valid_response_group)) != 0) {
            throw new InvalidResponseGroup();
        }

    }


    /**
     * Adds the information from additional table that cannot be obtained from JOIN query
     * @param array $additional_fields An array containing the name of additional fields
     * @param array $data The array containing data to be returned
     */
    private function addAdditionalInfo(array $additional_fields, array &$data) {
        // Gets an array of the additional queries for the current data source / table
        $additional_queries = $this->getAdditionalQueries();

        // For every tuple in the data get the additional data to be added
        // and append it to that tuple
        for ($i = 0; $i < count($data); $i++) {

            // Gets the model name of the current field
            $model_name = $data[$i]["model"];

            // For each query in the additional_queries array, perform it
            // and obtain the data
            foreach ($additional_fields as $field) {

                // Perform the query using the model name for current tuple
                $current_data = $this->getPdoConnection()->query(sprintf($additional_queries[$field], $model_name));

                if ($current_data->rowCount() != 0) {
                    // Append the obtained data under the name given in the additional_queries in the
                    // current tuple
                    $data[$i][$field] = $current_data->fetchAll(PDO::FETCH_COLUMN, 0);
                } else {
                    // If nothing was obtained store null
                    $data[$i][$field] = null;
                }

            }

        }

    }


    /**
     * Adds the requested response_group data to the data that is to be sent
     * @param string $response_group The value of the response_group parameter
     * @param array $data The array containing data to be returned
     * @throws ResponseGroupNotSupported If the offers response group is not supported throw ResponseGroupNotSupported
     */
    private function addResponseGroupInfo(string $response_group, array &$data) {

        // If the table does not have a tuple identifier
        // It means that no response group applies to it
        // Hence throw ResponseGroupNotSupported
        if ($this->getTupleIdentifier() == null) {
            throw new ResponseGroupNotSupported($this->getTableName());
        }

        // If the data is requested with price, add it.
        if ($response_group == "offers") {
            $this->addOffersResponse($data);
        }

        // If the data is requested with attributes, add them
        if ($response_group == "attributes") {
            $this->addAttributesResponse($data);
        }

        // If the data is requested with both offers and attributes return them
        if ($response_group == "attributes,offers" || $response_group == "offers,attributes") {
            $this->addAttributesResponse($data);
            $this->addOffersResponse($data);
        }

    }


    /**
     * Adds the offers data to the requested data
     * @param array $data The array containing data to be returned
     */
    private function addOffersResponse(array &$data) {

        // For every tuple search the prices table for the same model using the tuple_identifier
        // and add it to the tuple as a new object
        for ($i = 0; $i < count($data) ; $i++) {

            // Get the id column's name
            $id_column = key($data[$i]);

            // Gets the value of id column
            $current_id = $data[$i][$id_column];

            // A query to extract price data for the current tuple
            $query = "SELECT partner_name,price,link FROM prices WHERE model IN (SELECT " . $this->getTupleIdentifier() . " FROM " . $this->getTableName() . " WHERE $id_column = $current_id) ORDER BY price;";

            // Execute the query
            $q = $this->getPdoConnection()->query($query);

            if ($q->rowCount() != 0) {
                // Add the obtained data to the current tuple under the key 'offers'
                $data[$i]["offers"] = $q->fetchAll(PDO::FETCH_ASSOC);
            }
            else {
                // If nothing was obtained store null
                $data[$i]["offers"] = null;
            }

        }
    }


    /**
     * Adds the attributes data to the requested data
     * @param array $data The array containing data to be returned
     */
    private function addAttributesResponse(array &$data) {

        // For every tuple search the prices table for the same model using the tuple_identifier
        // and add it to the tuple as a new object
        for ($i = 0; $i < count($data) ; $i++) {

            // Get the id column's name
            $id_column = key($data[$i]);

            // Gets the value of id column
            $current_id = $data[$i][$id_column];

            // A query to extract product_data tuple for the current tuple
            $query = "SELECT link,rating,img_srcs FROM product_data WHERE model IN (SELECT " . $this->getTupleIdentifier() . " FROM " . $this->getTableName() . " WHERE $id_column = $current_id);";

            // Execute the query
            $q = $this->getPdoConnection()->query($query);

            if ($q->rowCount() != 0) {
                // Add the obtained data to the current tuple under the key 'attributes'
                $data[$i]["attributes"] = $q->fetch(PDO::FETCH_ASSOC);
            }
            else {
                // If nothing was obtained store null
                $data[$i]["attributes"] = null;
            }

        }

    }


    /**
     * A method that inserts the given values in the Data Source / Table
     * @param array $fields_values The fields to be inserted into the Data Source / Table
     * @return array An array containing the final result of insertion
     * @throws InsertionFailed If there is some problem with the insert operation throw InsertionFailed exception
     * @throws InvalidArgument If the fields specified are not valid throw InvalidArgument exception
     */
    function insertItem(array $fields_values): array {

        // Gets the fields from the request
        $fields = array_keys($fields_values);

        // Verifies the fields
        if(!$this->areValidFields($fields)) {
            throw new InvalidArgument();
        }

        // String to store the values
        $values = "";

        // For each pair in the associative array creates a
        // list of string separated by comma
        foreach ($fields_values as $value) {
            // If the value is string wrap in commas
            if (is_string($value)) {
                $values .= "\"$value\"" . ",";
                // Else if null is given add it
            } else if ($value === null) {
                $values .= "NULL" . ",";
                // Else just append to original string
            } else {
                $values .= $value . ",";
            }
        }

        // Finally trim the ending comma
        $values = trim($values, ",");

        // An empty array to hold the response
        $response = array();

        // Add the FeedbackMessage to the response
        $response["feedback"] = new FeedbackMessage();

        // Creates a MySQL query statement to obtain the desired fields
        $query = "INSERT INTO " . $this->getTableName() . "(" . implode(",",array_slice($this->getValidFields(), 1)) . ") VALUES($values);";

        // Perform the generated query
        $result = $this->getPdoConnection()->exec($query);

        // If no rows were affected throw InsertionFailed exception
        if ($result == 0) {
            throw new InsertionFailed($this->getTableName(), $fields_values);
        }

        // Sets the number of affected rows
        $response["feedback"]->setNumOfRows($result);

        // Return the final result
        return $response;

    }


    /**
     * A method that returns a single object from the queried DataSource
     * @param string $id ID of the tuple to return
     * @param array|null $fields Value of the 'fields' argument in form of array, that must be returned
     * @param string $response_group Specifies the attributes to return for an item
     * @return array An array containing a single tuple
     * @throws InvalidFieldsParameter If the fields specified are not correct throw InvalidFieldsParameter
     * @throws EmptyResultSet If there is no tuple with the given ID throw EmptyResultSet
     * @throws InvalidArgument If the id entered is not numeric throw InvalidArgument
     */
    function getSingleItem(string $id = null, array $fields = null, string $response_group = "info"): array {

        // If the entered id is not a number throw InvalidArgument
        if (!is_numeric($id)) {
            throw new InvalidArgument();
        }

        // Converts the string id to int
        $id = intval($id);

        // An empty array to hold the response
        $response = array();

        // Add the FeedbackMessage to the response
        $response['feedback'] = new FeedbackMessage();

        // Create a copy of the fields argument
        $fields_with_additional = $fields;

        // Sanitize the arguments: fields and response_group
        $fields = DataSource::sanitizeFields($this, $fields);
        self::sanitizeResponseGroup($response_group);

        // if all the additional fields are requested, hence get a name of all the additional fields
        if (count($fields_with_additional) != 0 && $fields_with_additional[0] == "all") {
            $additional_fields = array_keys($this->getAdditionalQueries());
        // If all the fields were not requested, get the additional_fields
        // by removing those fields which are valid for the data source / table
        } else {
            $additional_fields = array_values(array_diff($fields_with_additional, explode(",", $fields)));
        }

        // Gets the name of the id column
        $id_column = $this->getValidFields()[0];

        // Creates a MySQL query statement to obtain the desired fields
        $query = "SELECT $fields FROM " . $this->getTableName() . " WHERE $id_column = $id;";

        // Perform the generated query
        $result = $this->getPdoConnection()->query($query);

        // If no more results are obtained throw EmptyResultSet exception
        if ($result->rowCount() == 0) {
            throw new EmptyResultSet($id);
        }

        // Fetch the array containing the result
        $obtained_data = $result->fetchAll(PDO::FETCH_ASSOC);

        // Set the number of rows obtained
        $response["feedback"]->setNumOfRows($result->rowCount());

        // If there are additional queries to be performed, add the result
        // to the obtained data
        if (count($additional_fields) != 0 and strlen($additional_fields[0]) !== 0) {
            $this->addAdditionalInfo($additional_fields, $obtained_data);
        }

        // If the response_group contains value other than 'info'
        // than add the requested groups to the obtained_data
        if ($response_group != "info") {
            $this->addResponseGroupInfo($response_group, $obtained_data);
        }

        $response["data"] = $obtained_data;

        // return the final response
        return $response;
    }


    /**
     * A method that returns multiple objects from the queried DataSource
     * @param array|null $fields Value of the 'fields' argument in form of array, that must be returned
     * @param array|null $sort_by $fields Value of the 'sort_by' argument in form of array, that is used for sorting
     * @param string $order Specifies whether to sort ascending ASC (default) or descending DESC
     * @param int $from Offset in the DataSource from which data is returned
     * @param int $per_page Number of tuple to return
     * @param string $response_group Specifies the attributes to return for an item
     * @return array An array of tuple containing data as per the request
     * @throws EmptyResultSet If there is no tuple with the given ID throw EmptyResultSet exception
     * @throws InvalidOrder If the value of order parameter is incorrect throw InvalidOrder exception
     */
    function getMultipleItems(array $fields = null, array $sort_by = null, $order = "ASC", int $from = 0, int $per_page = Constants::PER_PAGE,
                              string $response_group = "info"): array {

        // An empty array to hold the response
        $response = array();

        // Add the FeedbackMessage to the response
        $response['feedback'] = new FeedbackMessage();

        // Create a copy of the fields argument
        $fields_with_additional = $fields;

        // Sanitize the arguments: fields , sort_by and response_group
        $fields = self::sanitizeFields($this, $fields);
        $sort_by = self::sanitizeSortBy($this, $sort_by);
        self::sanitizeResponseGroup($response_group);

        if ($order != "DESC" and $order != "ASC") {
            throw new InvalidOrder($order);
        }

        // if all the additional fields are requested, hence get a name of all the additional fields
        if (count($fields_with_additional) != 0 && $fields_with_additional[0] == "all") {
            $additional_fields = array_keys($this->getAdditionalQueries());
            // If all the fields were not requested, get the additional_fields
            // by removing those fields which are valid for the data source / table
        } else {
            $additional_fields = array_values(array_diff($fields_with_additional, explode(",", $fields)));
        }

        // Creates a MySQL query statement to obtain the desired fields
        if ($sort_by == null) {
            $query = "SELECT $fields FROM " . $this->getTableName() . " LIMIT $from, $per_page ;";
        } else {
            $query = "SELECT $fields FROM " . $this->getTableName() . " ORDER BY $sort_by $order LIMIT $from, $per_page ;";
        }

        // Perform the generated query
        $result = $this->getPdoConnection()->query($query);

        // If no more results are obtained throw EmptyResultSet exception
        if ($result->rowCount() == 0) {
            throw new EmptyResultSet();
        }

        // Fetch the array containing the result
        $obtained_data = $result->fetchAll(PDO::FETCH_ASSOC);

        // Set the number of rows obtained
        $response["feedback"]->setNumOfRows($result->rowCount());

        // If there are additional queries to be performed, add the result
        // to the obtained data
        if (count($additional_fields) != 0 && strlen($additional_fields[0]) != 0) {
            $this->addAdditionalInfo($additional_fields, $obtained_data);
        }

        // If the response_group contains value other than 'info'
        // than add the requested groups to the obtained_data
        if ($response_group != "info") {
            $this->addResponseGroupInfo($response_group, $obtained_data);
        }

        // Add the final result to the response
        $response["data"] = $obtained_data;

        // return the final response
        return $response;

    }


    /**
     * A method that updates the tuple with given ID
     * @param string $id ID of the tuple that needs to be updated
     * @param array $fields_values The fields and values that are valid for updating
     * @return array An array containing the final result of updating
     * @throws InvalidArgument If the fields specified are not valid throw InvalidArgument exception
     * @throws UpdateFailed If the update operation fails throw UpdateFailed exception
     */
    function updateItem(string $id, array $fields_values): array {

        // If the entered id is not a number throw InvalidArgument
        if (!is_numeric($id)) {
            throw new InvalidArgument();
        }

        // Converts the string id to int
        $id = intval($id);

        // Gets the fields from the request
        $fields = array_keys($fields_values);

        // Verifies the fields
        if(!$this->areValidFields($fields)) {
            throw new InvalidArgument();
        }

        // An empty array to hold the response
        $response = array();

        // Add the FeedbackMessage to the response
        $response["feedback"] = new FeedbackMessage();

        // A string to store the update query
        $update_str = "";

        // Creates a string from the associative array of the form
        // "emp_id=1,first_name="ram", ...,"
        foreach($fields_values as $key => $value) {
            //
            if (is_string($value)) {
                $update_str .= $key . "=\"" . $value . "\",";
                //
            } elseif ($value === null) {
                $update_str .= $key . "=NULL,";
                //
            } else {
                $update_str .= $key . "=" . $value . ",";
            }
        }

        // Removes the extra comma from the end
        $update_str = trim($update_str, ",");

        // Gets the name of the id column
        $id_column = $this->getValidFields()[0];

        // Creates a MySQL query statement to obtain the desired fields
        $query = "UPDATE " . $this->getTableName() . " SET $update_str" . " WHERE $id_column=" . $id . ";";

        // Perform the generated query
        $result = $this->getPdoConnection()->exec($query);

        // If no rows were affected throw UpdateFailed exception
        if ($result == 0) {
            throw new UpdateFailed($this->getTableName(), $id);
        }

        // Sets the number of affected rows
        $response["feedback"]->setNumOfRows($result);

        // Return the final result
        return $response;

    }


    /**
     * A method that deletes the tuple with given id
     * @param string $id ID of the tuple which needs to be deleted
     * @return array An array containing the final result of deletion
     * @throws DeletionFailed If the delete operation fails throw DeletionFailed exception
     * @throws InvalidID If the entered ID is not valid throw InvalidID exception
     */
    function deleteItem(string $id): array {

        // If the entered id is not a number throw InvalidArgument
        if (!is_numeric($id)) {
            throw new InvalidID($id);
        }

        // Converts the string id to int
        $id = intval($id);

        // An empty array to hold the response
        $response = array();

        // Add the FeedbackMessage to the response
        $response["feedback"] = new FeedbackMessage();

        // Gets the name of the id column
        $id_column = $this->getValidFields()[0];

        // Creates a MySQL query statement to obtain the desired fields
        $query = "DELETE FROM " . $this->getTableName() . " WHERE $id_column = $id;";

        // Perform the generated query
        $result = $this->getPdoConnection()->exec($query);

        // If the no numbers were affected throw DeletionFailed
        if ($result == 0) {
            throw new DeletionFailed($this->getTableName(), $id);
        }

        // Sets the number of affected rows
        $response["feedback"]->setNumOfRows($result);

        // Return the final result
        return $response;

    }
}