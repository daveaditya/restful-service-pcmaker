<?php

/** Namespace for the AccountInterface */
namespace PCMaker\Components;

/** Alias for the custom Exceptions */
use PCMaker\Exceptions\{
    AccountDeletionFailed, DeletionFailed, EmptyResultSet, InsertionFailed, InvalidArgument, InvalidFieldsParameter, InvalidID, InvalidPassword,
    InvalidSortParameter, InvalidUsername, NothingToDelete, PasswordResetFailed, UpdateFailed
};


/**
 * Interface AccountInterface
 * An interface that specifies the methods that are to be declared by the AccountSource class
 * @package PCMaker\Components
 */
interface AccountInterface {

    /**
     * A method that signup a dealer / technician
     * @param array $fields_values An array containing the values to enter in data source
     * @return array An array containing the final result of deletion
     * @throws InsertionFailed If the insert operation failed throw InsertionFailed exception
     * @throws InvalidArgument If the entered arguments are invalid throw InvalidArgument exception
     */
    function signup(array $fields_values): array;


    /**
     * A method that resets the password of given account
     * @param array $fields_values An array containing username and the new password
     * @return array An array containing the final result of reset password
     * @throws InvalidArgument If the required fields are not resent in the request throw InvalidArgument exception
     * @throws PasswordResetFailed If the update operation for password fails throw PasswordResetFailed exception
     */
    function resetPassword(array $fields_values): array;


    /**
     * A method to verify the credentials of user
     * @param array $fields_values An array containing the credentials to verify login with
     * @return array An array containing the final result of deletion
     * @throws InvalidArgument If the entered fields are invlid throw InvalidArgument exception
     * @throws InvalidPassword If the entered password does not match throw InvalidPassword exception
     * @throws InvalidUsername If the entered username does not exists in database throw InvalidUsername exception
     */
    function login(array $fields_values): array;


    /**
     * A method that returns a single object from the queried DataSource
     * @param string $username ID of the tuple to return
     * @param array|null $fields Value of the 'fields' argument in form of array, that must be returned
     * @return array An array containing a single tuple
     * @internal param string $response_group Specifies the attributes to return for an item
     */
    function getDetails(string $username, array $fields = null): array;


    /**
     * A method that returns multiple objects from the queried DataSource
     * @param array|null $fields Value of the 'fields' argument in form of array, that must be returned
     * @param array|null $sort_by $fields Value of the 'sort_by' argument in form of array, that is used for sorting
     * @param int $from Offset in the DataSource from which data is returned
     * @param int $per_page Number of tuple to return
     * @return array An array of tuple containing data as per the request
     * @throws InvalidFieldsParameter If the fields specified are not correct throw InvalidFieldsParameter
     * @throws InvalidSortParameter If the sort_by fields are not correct throw InvalidSortParameter
     * @throws EmptyResultSet If there is no tuple with the given ID throw EmptyResultSet
     */
    function getMultipleAccounts(array $fields = null, array $sort_by = null, int $from = 0, int $per_page = 0): array;


    /**
     * A method that updates the tuple with given ID
     * @param string $username ID of the tuple that needs to be updated
     * @param array $fields_values The fields and values that are valid for updating
     * @return array An array containing the final result of updating
     * @throws InvalidArgument If the fields specified are not valid throw InvalidArgument exception
     * @throws UpdateFailed If the update operation fails throw UpdateFailed exception
     */
    function updateAccountDetails(string $username, array $fields_values): array;


    /**
     * A method that deletes the tuple with given username
     * @param array $fields_values Username of the entity which needs to be deleted
     * @return array An array containing the final result of deletion
     * @throws AccountDeletionFailed If the delete operation fails throw DeletionFailed exception
     * @throws InvalidArgument If the fields are not valid throw InvalidArgument exception
     * @throws InvalidUsername If there is no account with given username throw InvalidUsername exception
     */
    function deleteAccount(array $fields_values): array;


    /**
     * A method that adds a new Technician to the organization
     * @param string $org_username Username of the organization
     * @param array $fields_values Associative array containing the "technician_username" to add
     * @return array An array containing the result of operation
     */
    function connectNewTechnician(string $org_username, array $fields_values): array;


    /**
     * A method that returns the list of all technicians registered under the given organization
     * @param string $username Username of the organization
     * @param int $from Offset in the DataSource from which data is returned
     * @param int $per_page Number of tuple to return
     * @return array An array containing the final result of reset password
     */
    function getConnectedTechnicians(string $username = "admin", int $from, int $per_page);


    /**
     * A method that returns a single connected technician
     * @param string $id ID of the tuple to be retrieved
     * @return array An array containing the single tuple
     * @throws EmptyResultSet If no tuple is present throw EmptyResultSet exception
     * @throws InvalidID If the entered ID is not valid throw InvalidID
     */
    function getSingleConnectedTechnician(string $id);


    /**
     * A method that removes a technician from the organization
     * @param string $org_username Username of the organization
     * @param string $technician_username Username of the technician
     * @return array An array containing the result of the operation
     * @throws DeletionFailed If the delete operation fails throw DeletionFailed exception
     * @throws NothingToDelete If there is no tuple with the given username throw NothingToDelete exception
     */
    function removeTechnician(string $org_username, string $technician_username): array;


    /**
     * A method that adds a new arrival request from the organization or technician
     * @param string $username Username of the technician or the organization
     * @param array $fields_values Values to be inserted into the new_arrival_request
     * @return array An array containing result of the insert operation
     * @throws InsertionFailed If the insertion fails throw InsertionFailed exception
     * @throws InvalidArgument If the arguments provided are not valid throw InvalidArgument exception
     * @throws InvalidUsername If the username of the organization or technician is not present in database throw InvalidUsername exception
     */
    function addNewArrival(string $username, array $fields_values): array;


    /**
     * A method that gets all the new arrival requests
     * @param string $username The username of the organization or technician that requested, if admin is specified return all
     * @return array An array containing all the new_arrival_request tuple
     * @throws EmptyResultSet If no data is present throw EmptyResultSet exception
     */
    function getNewArrivals(string $username = "admin"): array;


    /**
     * A method to delete a new_arrival_request tuple
     * @param string $id ID of the tuple to be deleted
     * @return array An array containing the response/result of the request
     * @throws DeletionFailed If the delete operation failed throw DeletionFailed exception
     * @throws InvalidID If the id specified is not valid throw InvalidID exception
     */
    function deleteNewArrival(string $id): array;


    /**
     * A method that adds new stock information
     * @param string $username Username of the organization or technician
     * @param array $fields_values An array containing the data to be inserted
     * @return array An array containing the response of the insertion
     * @throws InsertionFailed If the insert operation is unsuccessful throw InsertionFailed exception
     * @throws InvalidArgument If the argument specified is not valid throw InvalidArgument exception
     * @throws InvalidUsername If the username does not exist throw InvalidUsername
     */
    function addNewStockInfo(string $username, array $fields_values): array;


    /**
     * A method that gets a single stock tuple
     * @param string $username Username of the organization or technician
     * @param string $id ID of the tuple requested
     * @return array An array containing the requested tuple
     * @throws EmptyResultSet If there is no data than throw EmptyResultSet exception
     * @throws InvalidID If the id is not valid throw InvalidID exception
     */
    function getStockInfo(string $username,string $id): array;


    /**
     * A method that gets multiple tuple from stocks table
     * @param string $username Username of the organization or technician. If the username is "admin" return all the tuple
     * @param int $from Offset in the DataSource from which data is returned
     * @param int $per_page Number of tuple to return
     * @return array An array containing requested tuple
     */
    function getMultipleStockInfo(string $username = "admin", int $from, int $per_page): array;


    /**
     * A method that updates a stock info
     * @param string $id ID of the tuple to update
     * @param array $fields_values An array containing the data to be inserted
     * @return array An array containing the response of the operation
     * @throws InvalidArgument If the specified arguments are not valid throw InvalidArgument exception
     * @throws InvalidID If the ID is not valid throw InvalidID exception
     * @throws UpdateFailed If the update operation failed throw UpdateFailed exception
     */
    function updateStockInfo(string $id, array $fields_values): array;


    /**
     * A method that deletes a tuple from stock data source
     * @param string $id ID of the tuple to be deleted
     * @return array An array containing the response of operation
     * @throws DeletionFailed If the delete operation fails throw DeletionFailed exception
     * @throws InvalidID If the id provided is invalid throw InvalidID exception
     */
    function deleteStockInfo(string $id): array;

}