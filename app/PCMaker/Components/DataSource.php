<?php

/** Define namespace for the DataSource class */
namespace PCMaker\Components;

/** Alias for the Constants class containing various constants */
use PCMaker\Helpers\Constants;

/** Alias for the custom Exceptions */
use PCMaker\Exceptions\{
    InvalidFieldsParameter, InvalidSortParameter
};

/** Brings the PDO to current namespace */
use PDO;


/**
 * Class DataSource
 * This class deals with the requests data tuple
 * @package PCMaker\DataSource
 */
class DataSource {

    /** @var string Represents table from MySQL database corresponding to this class */
    private $table_name = "";

    /** @var string Default fields returned when fields are not provided */
    private $default_fields = "";

    /** @var string Specifies the column to perform sort on */
    private $default_sort = "";

    /** @var  array A container of all the column names */
    private $valid_fields;

    /** @var  array A container of all the column names eligible for sorting */
    private $valid_sort_fields;

    /** @var  array stores the names of fields of technician/organization tables */
    private $signup_fields;

    /** @var  string A string that specifies a mysql CONCAT to make a unique identifier */
    private $tuple_identifier = null;

    /** @var array An array containing additional queries in case of multiple table connections  */
    private $additional_queries = array();

    /** @var PDO Connection to database using PDO */
    private $pdo_connection;


    /**
     * DataSource constructor.
     * @param array $details An associative array containing keys table_name, default_fields, default_sort
     *              and valid_sort_fields
     */
    public function __construct(array $details) {
        // Creates a MySQL connection using PDO
        $this->setPdoConnection(new PDO(Constants::DSN, Constants::USER, Constants::PASSWORD), true);

        // Sets the table name for current component
        $this->setTableName($details["table_name"]);

        // Sets the default fields for the component
        $this->setDefaultFields($details["default_fields"]);

        // Set the default sort fields for the component
        $this->setDefaultSort($details["default_sort_fields"]);

        // Gets all the fields available in the table
        $this->setValidFields($this->getAllColumnNames());

        // Sets the valid sorting column names
        $this->setValidSortFields(explode(",", $details["valid_sort_fields"]));

        // If the tuple identifier is specified store it
        if (isset($details["signup_fields"])) {
            // Sets the tuple identifier value
            $this->setSignupFields(explode(",", $details["signup_fields"]));
        }

        // If the tuple identifier is specified store it
        if (isset($details["tuple_identifier"])) {
            // Sets the tuple identifier value
            $this->setTupleIdentifier($details["tuple_identifier"]);
        }

        // If the additional_queries are present store it
        if (isset($details["additional_queries"])) {

            // Sets the additional queries array for the current data source
            $this->setAdditionalQueries($details["additional_queries"]);

        }
    }


    /**
     * Returns an array of string containing column names of the table 'table_name'
     * @return array
     */
    private function getAllColumnNames() {
        // A query which returns 0 rows. But contains all the column names
        $rs = $this->getPdoConnection()->query('SELECT * FROM ' . $this->getTableName() . ' LIMIT 0;');

        // An array to contain all the column names
        $columns = array();

        // For every column in the result set fetch the column name
        // and store in the columns array
        for ($i = 0; $i < $rs->columnCount(); $i++) {

            // Gets the metadata of the current column
            $col = $rs->getColumnMeta($i);

            // Store the column names in a container, which is returned on completion
            $columns[] = $col['name'];
        }

        // return the column names
        return $columns;
    }


    /**
     * Returns the PDO object, which is connected to the database
     * @return PDO
     */
    public function getPdoConnection(): PDO {
        return $this->pdo_connection;
    }


    /**
     * Sets the PDO connection object for the data source / table
     * @param PDO $pdo_connection A PDO Connection to the PC Maker database
     * @param bool $developer_mode Specifies whether to handle exceptions explicitly or not
     */
    public function setPdoConnection(PDO $pdo_connection, bool $developer_mode = false) {
        // Creates a MySQL connection using PDO
        $this->pdo_connection = $pdo_connection;

        // If developer mode is enabled, throw PDOException
        // that might occur
        if ($developer_mode) {
            // Sets attributes on connection for error reporting.
            // Development purpose only.
            $this->pdo_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
    }


    /**
     * Returns the name of current component
     * @return string Data Source / Table
     */
    public function getTableName(): string {
        return $this->table_name;
    }


    /**
     * Sets the name of current data source
     * @param string $table_name Name of data source / table
     */
    public function setTableName(string $table_name) {
        $this->table_name = $table_name;
    }


    /**
     * Returns the string containing default fields for the data source
     * @return string Default fields for the data source
     */
    public function getDefaultFields(): string {
        return $this->default_fields;
    }


    /**
     * Sets the default fields to retrieve for $table_name
     * @param string $default_fields Default fields for the data source
     */
    public function setDefaultFields(string $default_fields) {
        $this->default_fields = $default_fields;
    }


    /**
     * Sets the additional queries array for the data source
     * @return array
     */
    public function getAdditionalQueries(): array {
        return $this->additional_queries;
    }


    /**
     * Gets the array of additional queries for the data source
     * @param array $additional_queries
     */
    public function setAdditionalQueries(array $additional_queries) {
        $this->additional_queries = $additional_queries;
    }


    /**
     * Returns the array containing valid fields of the data source / table
     * @return array Strings of column names for the data source / table
     */
    public function getValidFields(): array {
        return $this->valid_fields;
    }


    /**
     * Sets the valid fields for the data source / table
     * @param array $valid_fields Strings of column names for the data source / table
     */
    public function setValidFields(array $valid_fields) {
        $this->valid_fields = $valid_fields;
    }


    /**
     * Returns the default sort field for the data source / table
     * @return string
     */
    public function getDefaultSort(): ?string {
        return $this->default_sort;
    }


    /**
     * Sets the default sort field for the data source / table
     * @param string $default_sort
     */
    public function setDefaultSort(string $default_sort = null) {
        $this->default_sort = $default_sort;
    }

    /**
     * @return mixed
     */
    public function getSignupFields()
    {
        return $this->signup_fields;
    }

    /**
     * @param mixed $signup_fields
     */
    public function setSignupFields($signup_fields)
    {
        $this->signup_fields = $signup_fields;
    }




    /**
     * Returns the valid fields to sort on for the data source / table
     * @return array Strings of valid field names
     */
    public function getValidSortFields(): array {
        return $this->valid_sort_fields;
    }

    /**
     * Sets the valid fields to sort on for the data source / table
     * @param array $valid_sort Strings of valid fields
     */
    public function setValidSortFields(array $valid_sort) {
        $this->valid_sort_fields = $valid_sort;
    }


    /**
     * Checks whether the fields are valid or not
     * @param array $fields an array containing all the field names
     * @return bool return true is all fields are valid else return false
     */
    protected function areValidFields(array $fields): bool {
        if (count($this->getAdditionalQueries()) != 0) {
            // Gets the valid fields which are all the columns of current table
            // and the new columns from other tables if any
            $valid_fields = array_merge($this->getValidFields(), array_keys($this->getAdditionalQueries()));
        } else {
            // Gets the valid fields
            $valid_fields = $this->getValidFields();
        }

        // If there are elements which are in input fields but not in valid_fields
        // Return false
        if (count(array_diff($fields, $valid_fields)) != 0) {
            return false;
        }

        // Return true if every field is in the valid_fields array
        return true;
    }


    /**
     * Checks whether the fields are valid or not
     * @param array $fields an array containing all the field names
     * @return bool return true is all fields are valid else return false
     */
    protected function areValidSortFields(array $fields): bool {
        // Gets the valid sort fields for the data source
        $valid_sort_fields = $this->getValidSortFields();

        // If there are elements which are in input fields but not in valid_sort_fields
        // Return false
        if (count(array_diff($fields, $valid_sort_fields)) != 0) {
            return false;
        }

        // Return true if every field is in the valid_sort_fields array
        return true;
    }


    /**
     * Checks whether the fields entered are valid or not
     * @param DataSource object
     * @param array $fields an array containing the field names
     * @return string csv representation of the fields
     * @throws InvalidFieldsParameter because the field name might be incorrect
     */
    protected static function sanitizeFields(DataSource $object, array $fields): string {

        // If fields are null or empty use the default_fields
        if ($fields == null || count($fields) == 0 || (count($fields) == 1 && $fields['0'] == "")) {
            // Gets the default fields array
            return $object->getDefaultFields();

            // if special keyword 'all' is used, return all fields
        } elseif (count($fields) == 1 && $fields['0'] == 'all') {
            return implode(",", $object->getValidFields());

            // else check if the requested fields are valid or not
        } elseif ($object->areValidFields($fields)) {

            if (count($object->getAdditionalQueries()) != 0) {
                return implode(",", array_intersect($fields, array_diff($fields, array_keys($object->getAdditionalQueries()))));
            }

            //Converts the fields array into a single csv line to use in query
            return implode(",", $fields);

            // Everything fails means the field names are invalid. Throw an InvalidFieldsParameter exception
        } else {
            throw new InvalidFieldsParameter();
        }

    }


    /**
     * Checks whether the fields entered for sorting are valid or not
     * @param DataSource object
     * @param array $sort_by an array containing the field names
     * @return string csv representation of the fields
     * @throws InvalidSortParameter because the field name might be incorrect
     */
    protected static function sanitizeSortBy(DataSource $object, array $sort_by): ?string {

        // If fields are null or empty use the default_sort
        if ($sort_by == null || count($sort_by) == 0 || (count($sort_by) == 1 && $sort_by['0'] == '')) {
            // Gets the default fields array
            return $object->getDefaultSort();

            // Else, check if the given fields are valid, if true convert to csv and return
        } elseif ($object->areValidSortFields($sort_by)) {

            //Converts the fields array into a single csv line to use in query
            return implode(",", $sort_by);

            // Everything fails means the field names are invalid. Throw an IllegalFieldsException
        } else {
            throw new InvalidSortParameter();
        }

    }


    /**
     * Sets the string containing CONCAT function to uniquely identify a tuple
     * @return string
     */
    protected function getTupleIdentifier(): string {
        return $this->tuple_identifier;
    }


    /**
     * Returns the string containing CONCAT function to uniquely identify a tuple
     * @param string $tuple_identifier
     */
    protected function setTupleIdentifier(string $tuple_identifier = null) {
        $this->tuple_identifier = $tuple_identifier;
    }

}