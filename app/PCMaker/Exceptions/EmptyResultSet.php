<?php

/** Namespace for the EmptyResultSet exception */
namespace PCMaker\Exceptions;

/** Brings Exception to the current namespace */
use Exception;


/**
 * Class EmptyResultSet
 * An Exception to denote that the requested returned zero data
 * @package PCMaker\Exceptions
 */
class EmptyResultSet extends Exception {

    /**
     * EmptyResultSet constructor.
     * @param int $id
     */
    public function __construct(int $id = 0) {
        if ($id != 0) {
            $message = "No result found with given id: $id";
        } else {
            $message = "No more results";
        }
        parent::__construct($message, 0, null);
    }

}