<?php

/** Namespace for the PasswordResetFailed exception */
namespace PCMaker\Exceptions;

/** Brings Exception to the current namespace */
use Exception;


/**
 * Class PasswordResetFailed
 * @package PCMaker\Exceptions
 */
class PasswordResetFailed extends Exception {

    /**
     * PasswordResetFailed constructor.
     */
    public function __construct() {

        $message = "Password reset failed!";

        parent::__construct($message, 0, null);
    }

}