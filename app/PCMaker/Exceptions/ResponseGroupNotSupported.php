<?php

/** Defines namespace for the ResponseGroupNotSupported class */
namespace PCMaker\Exceptions;

/** Brings Exception class to the current namespace */
use Exception;


/**
 * Class ResponseGroupNotSupported
 * An Exception to denote that the value of the response_group parameter is invalid
 * @package PCMaker\Exceptions
 */
class ResponseGroupNotSupported extends Exception {

    /**
     * InvalidArgument constructor.
     * @param string $table_name Name of the table that caused the exception
     */
    public function __construct(string $table_name) {
        // Create a custom message
        $message = "Response Group not supported by : " . $table_name;

        // Calls the parent class constructor
        parent::__construct($message, 0, null);
    }

}