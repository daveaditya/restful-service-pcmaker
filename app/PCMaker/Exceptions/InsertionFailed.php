<?php

/** Namespace for the InsertionFailed exception */
namespace PCMaker\Exceptions;

/** Brings Exception to the current namespace */
use Exception;


/**
 * Class InsertionFailed
 * @package PCMaker\Exceptions
 */
class InsertionFailed extends Exception {

    /**
     * InsertionFailed constructor.
     * @param string $table_name
     * @param array $values
     */
    public function __construct(string $table_name, array $values) {

        $message = "Insertion failed for \"$table_name\" with values \"" . implode(",", $values) ."\"";

        parent::__construct($message, 0, null);
    }

}