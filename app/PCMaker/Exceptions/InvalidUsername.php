<?php

/** Namespace for the InvalidUsername exception */
namespace PCMaker\Exceptions;

/** Brings Exception to the current namespace */
use Exception;


/**
 * Class InvalidUsername
 * @package PCMaker\Exceptions
 */
class InvalidUsername extends Exception {

    /**
     * InvalidUsername constructor.
     * @param string $username
     */
    public function __construct(string $username) {

        $message = "Wrong username entered \"$username\"";

        parent::__construct($message, 0, null);
    }

}