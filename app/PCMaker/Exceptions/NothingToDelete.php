<?php

/** Namespace for the NothingToDelete exception */
namespace PCMaker\Exceptions;

/** Brings Exception to the current namespace */
use Exception;


/**
 * Class NothingToDelete
 * @package PCMaker\Exceptions
 */
class NothingToDelete extends Exception {

    /**
     * NothingToDelete constructor.
     */
    public function __construct() {

        $message = "No data exists with given parameters";

        parent::__construct($message, 0, null);
    }

}