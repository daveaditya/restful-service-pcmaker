<?php

/** Namespace for the UsernameTaken exception */
namespace PCMaker\Exceptions;

/** Brings Exception to the current namespace */
use Exception;


/**
 * Class UsernameTaken
 * @package PCMaker\Exceptions
 */
class UsernameTaken extends Exception {

    /**
     * SignupFailed constructor.
     * @param string $username
     */
    public function __construct(string $username) {

        $message = "The entered user already exists: $username";

        parent::__construct($message, 0, null);
    }

}