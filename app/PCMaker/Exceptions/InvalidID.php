<?php

/** Namespace for the InvalidID exception */
namespace PCMaker\Exceptions;

/** Brings Exception to the current namespace */
use Exception;


/**
 * Class InvalidID
 * @package PCMaker\Exceptions
 */
class InvalidID extends Exception {

    /**
     * InvalidID constructor.
     * @param int $id
     */
    public function __construct(int $id) {

        $message = "Entered ID \"$id\" is invalid";

        parent::__construct($message, 0, null);
    }

}