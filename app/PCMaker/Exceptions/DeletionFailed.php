<?php

/** Namespace for the DeletionFailed exception */
namespace PCMaker\Exceptions;

/** Brings Exception to the current namespace */
use Exception;


/**
 * Class DeletionFailed
 * @package PCMaker\Exceptions
 */
class DeletionFailed extends Exception {

    /**
     * DeletionFailed constructor.
     * @param string $table_name
     * @param int $id
     */
    public function __construct(string $table_name = null, int $id = null) {

        if ($table_name === null and $id === null) {
            $message = "Deletion failed";
        } else {
            $message = "Deletion failed for \"$table_name\" with ID \"$id\"";
        }

        parent::__construct($message, 0, null);
    }

}