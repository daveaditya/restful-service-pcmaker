<?php

/** Namespace for the UpdateFailed exception */
namespace PCMaker\Exceptions;

/** Brings Exception to the current namespace */
use Exception;


/**
 * Class UpdateFailed
 * @package PCMaker\Exceptions
 */
class UpdateFailed extends Exception {

    /**
     * UpdateFailed constructor.
     * @param string $table_name
     * @param int $id
     */
    public function __construct(string $table_name, int $id) {

        $message = "Update failed for \"$table_name\" with : ID \"$id\"";

        parent::__construct($message, 0, null);
    }

}