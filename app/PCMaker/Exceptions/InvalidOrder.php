<?php

/** Defines namespace for the InvalidOrder class */
namespace PCMaker\Exceptions;

/** Brings Exception class to the current namespace */
use Exception;


/**
 * Class InvalidOrder
 * An Exception to denote that the value of order argument is invalid
 * @package PCMaker\Exceptions
 */
class InvalidOrder extends Exception {

    /**
     * InvalidArgument constructor.
     * @param string $order Order request that resulted in InvalidOrder exception
     */
    public function __construct(string $order) {
        // Create a custom message
        $message = "The requested order is incorrect: $order";

        // Calls the parent class constructor
        parent::__construct($message, 0, null);
    }

}