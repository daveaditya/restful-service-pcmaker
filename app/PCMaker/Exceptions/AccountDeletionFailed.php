<?php

/** Namespace for the AccountDeletionFailed exception */
namespace PCMaker\Exceptions;

/** Brings Exception to the current namespace */
use Exception;


/**
 * Class AccountDeletionFailed
 * @package PCMaker\Exceptions
 */
class AccountDeletionFailed extends Exception {

    /**
     * AccountDeletionFailed constructor.
     * @param string $table_name
     * @param string $username
     */
    public function __construct(string $table_name, string $username) {

        $message = "Deletion failed for \"$table_name\" with ID \"$username\"";

        parent::__construct($message, 0, null);
    }

}