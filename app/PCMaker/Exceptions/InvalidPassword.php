<?php

/** Namespace for the InvalidPassword exception */
namespace PCMaker\Exceptions;

/** Brings Exception to the current namespace */
use Exception;


/**
 * Class InvalidPassword
 * @package PCMaker\Exceptions
 */
class InvalidPassword extends Exception {

    /**
     * InvalidPassword constructor.
     * @param string $username
     * @param string $password
     */
    public function __construct(string $username, string $password) {

        $message = "Wrong password \"$password\" entered for \"$username\"";

        parent::__construct($message, 0, null);
    }

}