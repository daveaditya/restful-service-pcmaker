<?php

/** Defines namespace for the InvalidArgument class */
namespace PCMaker\Exceptions;

/** Brings Exception class to the current namespace */
use Exception;


/**
 * Class InvalidArgument
 * An Exception to denote that the arguments that specified in the request are not supported, hence invalid
 * @package PCMaker\Exceptions
 */
class InvalidArgument extends Exception {

    /**
     * InvalidArgument constructor.
     */
    public function __construct() {
        // Create a custom message
        $message = "The request contains invalid arguments";

        // Calls the parent class constructor
        parent::__construct($message, 0, null);
    }

}