<?php

/** Defines namespace for InvalidSortParameter class */
namespace PCMaker\Exceptions;

/** Brings Exception to the current namespace */
use Exception;

/**
 * Class InvalidSortParameter
 * An Exception to denote that the arguments provided for the sort_by parameter
 * are invalid
 * table
 * @package PCMaker\Exceptions
 */
class InvalidSortParameter extends Exception {

    /**
     * InvalidFields constructor.
     */
    public function __construct() {
        // Create a custom message
        $message = "Value of sort_by argument is not valid.";

        // Calls the parent class constructor
        parent::__construct($message, 0, null);
    }

}