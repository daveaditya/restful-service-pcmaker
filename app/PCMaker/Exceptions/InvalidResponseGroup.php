<?php

/** Defines namespace for the InvalidResponseGroup class */
namespace PCMaker\Exceptions;

/** Brings Exception class to the current namespace */
use Exception;


/**
 * Class InvalidResponseGroup
 * An Exception to denote that the value of the response_group parameter is invalid
 * @package PCMaker\Exceptions
 */
class InvalidResponseGroup extends Exception {

    /**
     * InvalidArgument constructor.
     */
    public function __construct() {
        // Create a custom message
        $message = "One or more response groups are not valid";

        // Calls the parent class constructor
        parent::__construct($message, 0, null);
    }

}