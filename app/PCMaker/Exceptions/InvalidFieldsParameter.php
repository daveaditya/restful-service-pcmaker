<?php

/** Defines namespace for InvalidFieldsParameter class */
namespace PCMaker\Exceptions;

/** Brings Exception to the current namespace */
use Exception;

/**
 * Class InvalidFields
 * An Exception to denote that the fields requested do not comply with the
 * requirements of the data source
 * @package PCMaker\Exceptions
 */
class InvalidFieldsParameter extends Exception {

    /**
     * InvalidFieldsParameter constructor.
     */
    public function __construct() {
        // Create a custom message
        $message = "One or more fields were not valid";

        // Calls the parent class constructor
        parent::__construct($message, 0, null);
    }

}