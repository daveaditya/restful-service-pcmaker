<?php

/** Defines namespace for InvalidRequest class */
namespace PCMaker\Exceptions;

/** Brings Exception to the current namespace */
use Exception;

/**
 * Class InvalidRequest
 * An Exception to denote that the requested data source does not exist
 * @package PCMaker\Exceptions
 */
class InvalidRequest extends Exception {

    /** @var string Stores the name of field that cause this Exception */
    private $table_name = '';

    /**
     * InvalidRequest constructor.
     * @param $table_name string Name of the field that caused this Exception
     */
    public function __construct(string $table_name) {
        // Store the name of the field
        $this->table_name = $table_name;

        // Create a custom message
        $message = "The requested data does not exists: " . $table_name;

        // Calls the parent class constructor
        parent::__construct($message, 0, null);
    }

}