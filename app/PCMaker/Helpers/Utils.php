<?php

/** Defines namespace for Utils class */
namespace PCMaker\Helpers;

/**
 * Contains utility functions to help perform certain tasks
 */
class Utils {

    /**
     * A function that converts an array into a csv line
     * Example:
     * arrayToCsvSingle(array("Apple","Google","Microsoft))
     * returns "Apple,Google,Microsoft"
     * @param array $items
     * @return string|null
     */
    public static function arrayToCsvSingle(array $items): string {

        // Checks the $items parameter for null or empty
        if ($items == null || count($items) == 0) {
            print_r('An argument may have helped.');
            return null;
        }

        // Store the intermediate results
        $output = '';

        // For every string in the array, add a comma and append
        // the string to the output variable
        foreach ($items as $item) {
            $output .= $item . ',';
        }

        // Removes the last comma and returns the string
        $output = rtrim($output, ',');

        // Return the perfectly generated csv line
        return $output;
    }


    /**
     * A function that checks the presence of elements of the source array
     * in the ideal array
     * @param array $source The array whose elements are needed to be checked
     * @param array $ideal The array containing only the desired elements
     * @return bool Returns true if the source array's elements are present in the ideal array
     *              else returns false
     */
    public static function validArgsChecker(array $source, array $ideal): bool {

        // Compare the source array against the ideal array
        $diff = array_diff($source, $ideal);

        // If there are elements present in source which are not in ideal
        // They are stored in the diff variable
        // Hence, if diff has some values return false
        if (count($diff) != 0) {
            return false;
        }

        // If everything element is present in the ideal array return true
        return true;
    }


    /**
     * A function to convert a single dimensional array into a SQL compatible
     * string with regards to the data type
     * @param array $data A single dimensional array
     * @return string SQL String representation of the array
     */
    public static function assocArrayToSQLString(array $data): string {

        // String to store the values
        $values = "";

        // For each pair in the associative array creates a
        // list of string separated by comma
        foreach ($data as $value) {
            // If the value is string wrap in commas
            if (is_string($value)) {
                $values .= "\"$value\"" . ",";
                // Else if null is given add it
            } else if ($value === null) {
                $values .= "NULL" . ",";
                // Else just append to original string
            } else {
                $values .= $value . ",";
            }
        }

        // Finally trim the ending comma
        $values = trim($values, ",");

        // Returns the final string
        return $values;

    }


    /**
     * A function to convert a associative array into a SQL compatible
     * string with regards to the data type and keys
     * @param array $data A single dimensional array
     * @return string SQL String representation of the array
     */
    public static function assocArrayToParams(array $data): string {

        // String to store the values
        $update_str = "";

        // Creates a string from the associative array of the form
        // "emp_id=1,first_name="ram", ...,"
        foreach($data as $key => $value) {
            //
            if (is_string($value)) {
                $update_str .= $key . "=\"" . $value . "\",";
                //
            } elseif ($value === null) {
                $update_str .= $key . "=NULL,";
                //
            } else {
                $update_str .= $key . "=" . $value . ",";
            }
        }

        // Removes the extra comma from the end
        $update_str = trim($update_str, ",");

        // Returns the final string
        return $update_str;

    }
}