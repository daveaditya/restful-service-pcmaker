<?php

/** Defines namespace for FeedbackMessage class */
namespace PCMaker\Helpers;

/**
 * A class to represent the structure of Feedback to give to the consumer.
 * Details of error message, pagination, and effect of an operation can be
 * given.
 */
class FeedbackMessage
{

    /** @const A constant to represent the status of response, either SUCCESS or FAILURE */
    const SUCCESS = 'SUCCESS';
    const FAILURE = 'FAILURE';

    /** @var  int Represents the SUCCESS or FAILURE of the processing of request */
    public $response = self::SUCCESS;

    /** @var  string Type of error that occurred during processing, if any */
    public $error_type = null;

    /** @var  string Description of the error that might have occurred */
    public $error_message = null;

    /** @var  string Link that was sent to generate the response */
    public $current_link = null;

    /** @var  string Link to the next set of data*/
    public $next_page = null;

    /** @var  string Number of results returned per page */
    public $per_page = Constants::PER_PAGE;

    /** @var  int Number of rows obtained by processing the request */
    public $num_of_rows = null;


    /**
     * A default do-nothing FeedbackMessage constructor.
     */
    function __construct() {
    }


    /**
     * Returns the response value for the FeedbackMessage
     * @return string
     */
    public function getResponse(): string {
        return $this->response;
    }


    /**
     * Sets the response value for the FeedbackMessage
     * @param string $response
     */
    public function setResponse(string $response) {
        $this->response = $response;
    }


    /**
     * Returns the error_type of the FeedbackMessage, if any.
     * Else returns null.
     * It is the name of the class of Exception that might have occurred.
     * @return string|null
     */
    public function getErrorType(): string {
        return $this->error_type;
    }


    /**
     * Sets the error_type of the FeedbackMessage.
     * It is the name of the class of Exception that might have occurred.
     * If no Exception occurred set it to null
     * @param string $error_type
     */
    public function setErrorType(string $error_type) {
        $this->error_type = $error_type;
    }


    /**
     * Returns the description of the Exception that might have occurred.
     * If no Exception occurred it returns null
     * @return string|null
     */
    public function getErrorMessage(): string {
        return $this->error_message;
    }


    /**
     * Sets the description of the Exception that might have occurred.
     * If no Exception occurred set it to null
     * @param string $error_message
     */
    public function setErrorMessage(string $error_message) {
        $this->error_message = $error_message;
    }


    /**
     * Returns the URI that led to the generation of the FeedbackMessage.
     * @return string
     */
    public function getCurrentLink(): string {
        return $this->current_link;
    }


    /**
     * Sets the URI that led to the generation of the FeedbackMessage.
     * @param string $current_link
     */
    public function setCurrentLink(string $current_link) {
        $this->current_link = $current_link;
    }


    /**
     * Returns the URI that points to the next page/section of response
     * @return string
     */
    public function getNextPage(): string {
        return $this->next_page;
    }


    /**
     * Sets the URI that points to the next page/section of response
     * @param string $next_page
     */
    public function setNextPage(string $next_page) {
        $this->next_page = $next_page;
    }


    /**
     * Returns the number of data object returned per page/section
     * @return string
     */
    public function getPerPage(): string {
        return $this->per_page;
    }


    /**
     * Sets the number of data object returned per page/section
     * @param string $per_page
     */
    public function setPerPage(string $per_page) {
        $this->per_page = $per_page;
    }


    /**
     * Returns the number of rows returned by the request
     * The number might be same as per_page, but differs when
     * the request is less than the default per page limit.
     * @return int
     */
    public function getNumOfRows(): int {
        return $this->num_of_rows;
    }


    /**
     * Sets the number of rows returned by the request
     * The number might be same as per_page, but differs when
     * the request is less than the default per page limit.
     * @param int $no_of_rows
     */
    public function setNumOfRows(int $no_of_rows) {
        $this->num_of_rows = $no_of_rows;
    }

}