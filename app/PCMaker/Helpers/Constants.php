<?php

/** Defines namespace for Constants class */
namespace PCMaker\Helpers;

/**
 * A class which hold various constants to be used with the database operations.
 */
final class Constants {

    /** @var string Represents the host of MySQL database server */
    const HOST = "localhost";

    /** @var string Name of the database to access */
    const DATABASE = "pc_maker";

    /** @var string Username for MySQL server */
    const USER = "jump";

    /** @var string Password for defined username */
    const PASSWORD = "secret";

    /** @var string Character Set to be used with MySQL connection */
    const CHARSET = "utf8";

    /** @var string DSN for establishing connection to database */
    const DSN = "mysql:host=" . self::HOST . ";dbname=" . self::DATABASE . ";charset=" . self::CHARSET;

    /** @var int Number of rows to return i.e. LIMIT */
    const PER_PAGE = 25;

    /** @var array The permitted arguments for the GET request */
    const valid_args = array(
        "GET" => array("fields", "sort_by", "from", "per_page", "response_group", "order")
    );

    /** @var array The permitted response group values */
    const valid_response_group = array(
        "offers", "info", "attributes"
    );

    /**
     * @var array Associative array containing valid component table names with default_fields, default_sort_fields,
     * tuple identifier string, and the permitted valid sort fields. Also contained is additional query if any.
     */
    const HARDWARE_RELATED_TABLES = array(
        "amd_chipsets" => array(
            "table_name" => "amd_chipset",
            "default_fields" => "amd_chipset_id,model,supported_cpus",
            "default_sort_fields" => "amd_chipset_id",
            "valid_sort_fields_fields" => "amd_chipset_id,model",
            "tuple_identifier" => null
        ),
        "amd_processors" => array(
            "table_name" => "amd_processor",
            "default_fields" => "amd_processor_id,model,family,no_of_cpu_cores,base_frequency",
            "default_sort_fields" => "amd_processor_id",
            "valid_sort_fields" => "amd_processor_id,model",
            "tuple_identifier" => "CONCAT(amd_processor.line,' ',amd_processor.model)"
        ),
        "cabinets" => array(
            "table_name" => "cabinet",
            "default_fields" => "cabinet_id,company_name,model,type",
            "default_sort_fields" => "cabinet_id",
            "valid_sort_fields" => "cabinet_id,model",
            "tuple_identifier" => "CONCAT(company_name,' ',model)",
            "additional_queries" => array(
                "mbd_form_factors" => "SELECT mbd_form_factor_name FROM cabinet_mbd_form_factor WHERE cabinet_model=\"%s\";",
                "psu_form_factors" => "SELECT psu_form_factor_name FROM cabinet_psu_form_factor WHERE cabinet_model=\"%s\";",
                "supported_usb_vers" => "SELECT usb_version FROM cabinet_usb_support WHERE cabinet_model=\"%s\";"
            )
        ),
        "cabinet_mbd_form_factors" => array(
            "table_name" => "cabinet_mbd_form_factor",
            "default_fields" => "*",
            "default_sort_fields" => "cabinet_mbd_form_factor_id",
            "valid_sort_fields" => "cabinet_mbd_form_factor_id,cabinet_model,mbd_form_factor_name",
            "tuple_identifier" => null
        ),
        "cabinet_psu_form_factors" => array(
            "table_name" => "cabinet_psu_form_factor",
            "default_fields" => "*",
            "default_sort_fields" => "cabinet_psu_form_factor_id",
            "valid_sort_fields" => "cabinet_psu_form_factor_id,cabinet_model,psu_form_factor_name",
            "tuple_identifier" => null
        ),
        "cabinet_types" => array(
            "table_name" => "cabinet_type",
            "default_fields" => "*",
            "default_sort_fields" => null,
            "valid_sort_fields" => null,
            "tuple_identifier" => null
        ),
        "cabinet_usb_supports" => array(
            "table_name" => "cabinet_usb_support",
            "default_fields" => "*",
            "default_sort_fields" => "cabinet_usb_support_id",
            "valid_sort_fields" => "cabinet_usb_support_id,cabinet_model,usb_version",
            "tuple_identifier" => null
        ),
        "chasis_coolers" => array(
            "table_name" => "chasis_cooler",
            "default_fields" => "chasis_cooler_id,company_name,model,size",
            "default_sort_fields" => "chasis_cooler_id",
            "valid_sort_fields" => "chasis_cooler_id,size,min_rpm,max_rpm",
            "tuple_identifier" => "CONCAT(company_name,' ',model)"
        ),
        "companies" => array(
            "table_name" => "company",
            "default_fields" => "*",
            "default_sort_fields" => "company_name",
            "valid_sort_fields" => "company_name",
            "tuple_identifier" => null
        ),
        "company_components" => array(
            "table_name" => "company_component",
            "default_fields" => "*",
            "default_sort_fields" => null,
            "valid_sort_fields" => null,
            "tuple_identifier" => null
        ),
        "components" => array(
            "table_name" => "component",
            "default_fields" => "*",
            "default_sort_fields" => "component_name",
            "valid_sort_fields" => "component_name",
            "tuple_identifier" => null
        ),
        "configurations" => array(
            "table_name" => "configuration",
            "default_fields" => "component_name,product_name",
            "default_sort_fields" => "configuration_id",
            "valid_sort_fields" => "configuration_id",
            "tuple_identifier" => null
        ),
        "cpu_coolers" => array(
            "table_name" => "cpu_cooler",
            "default_fields" => "cpu_cooler_id,company_name,model,type",
            "default_sort_fields" => "cpu_cooler_id",
            "valid_sort_fields" => "cpu_cooler_id,min_rpm,max_rpm",
            "tuple_identifier" => "CONCAT(company_name,' ',model)",
            "additional_queries" => array(
                "supported_sockets" => "SELECT socket_name FROM cpu_cooler_sockets WHERE model_name=\"%s\";",
            )
        ),
        "cpu_cooler_sockets" => array(
            "table_name" => "cpu_cooler_socket",
            "default_fields" => "*",
            "default_sort_fields" => "cpu_cooler_sockets_id",
            "valid_sort_fields" => "cpu_cooler_sockets_id,model_name,socket_name",
            "tuple_identifier" => null
        ),
        "fan_sizes" => array(
            "table_name" => "fan_size",
            "default_fields" => "*",
            "default_sort_fields" => null,
            "valid_sort_fields" => null,
            "tuple_identifier" => null
        ),
        "gpus_amd" => array(
            "table_name" => "gpu_amd",
            "default_fields" => "gpu_amd_id,model,family,gpu_boost,memory_size",
            "default_sort_fields" => "gpu_amd_id",
            "valid_sort_fields" => "gpu_amd_id,gpu_boost,memory_size,power_consumption",
            "tuple_identifier" => null
        ),
        "gpus_nvidia" => array(
            "table_name" => "gpu_nvidia",
            "default_fields" => "gpu_nvidia_id,model,base_clock,cuda_cores,memory_size",
            "default_sort_fields" => "gpu_nvidia_id",
            "valid_sort_fields" => "gpu_nvidia_id,base_clock,cuda_cores,memory_size",
            "tuple_identifier" => null
        ),
        "graphics_cards" => array(
            "table_name" => "graphics_card",
            "default_fields" => "graphics_card_id,gpu_model,company_name,model,base_clock,memory_size",
            "default_sort_fields" => "graphics_card_id",
            "valid_sort_fields" => "graphics_card_id,base_clock,memory_size",
            "tuple_identifier" => "CONCAT(company_name,' ',model)"
        ),
        "headphones" => array(
            "table_name" => "headphone",
            "default_fields" => "headphone_id,company_name,model,medium,type",
            "default_sort_fields" => "headphone_id",
            "valid_sort_fields" => "headphone_id,company_name,model",
            "tuple_identifier" => "CONCAT(company_name,' ',model)"
        ),
        "intel_chipsets" => array(
            "table_name" => "intel_chipset",
            "default_fields" => "intel_chipset_id,model,lithography,thermal_design_power",
            "default_sort_fields" => "intel_chipset_id",
            "valid_sort_fields" => "lithography,no_of_usb3,no_of_usb2",
            "tuple_identifier" => null
        ),
        "intel_processors" => array(
            "table_name" => "intel_processor",
            "default_fields" => "intel_processor_id,model,base_frequency,cache_size,lithography,no_of_cores,no_of_threads,memory_type",
            "default_sort_fields" => "intel_processor_id",
            "valid_sort_fields" => "intel_processor_id,base_frequency,lithography,no_of_cores,no_of_threads,max_memory_support",
            "tuple_identifier" => "model"
        ),
        "interfaces" => array(
            "table_name" => "interfaces",
            "default_fields" => "*",
            "default_sort_fields" => "interface_name",
            "valid_sort_fields" => null,
            "tuple_identifier" => null
        ),
        "keyboard_mouses" => array(
            "table_name" => "keyboard_mouse",
            "default_fields" => "key_mouse_id,company_name,model,included_devices,medium",
            "default_sort_fields" => "key_mouse_id",
            "valid_sort_fields" => "key_mouse_id,medium",
            "tuple_identifier" => "CONCAT(company_name,' ',model)"
        ),
        "master_chipsets" => array(
            "table_name" => "master_chipset",
            "default_fields" => "*",
            "default_sort_fields" => null,
            "valid_sort_fields" => null,
            "tuple_identifier" => null
        ),
        "master_gpus" => array(
            "table_name" => "master_gpu",
            "default_fields" => "*",
            "default_sort_fields" => null,
            "valid_sort_fields" => null,
            "tuple_identifier" => null
        ),
        "master_inventories" => array(
            "table_name" => "master_inventory",
            "default_fields" => "*",
            "default_sort_fields" => "company_name",
            "valid_sort_fields" => "company_name,component_name",
            "tuple_identifier" => null
        ),
        "master_processors" => array(
            "table_name" => "master_processor",
            "default_fields" => "*",
            "default_sort_fields" => null,
            "valid_sort_fields" => null,
            "tuple_identifier" => null
        ),
        "mbd_socket_supports" => array(
            "table_name" => "mbd_socket_support",
            "default_fields" => "*",
            "default_sort_fields" => "mbd_socket_support_id",
            "valid_sort_fields" => "mbd_socket_support_id",
            "tuple_identifier" => null
        ),
        "mbd_usb_supports" => array(
            "table_name" => "mbd_usb_support",
            "default_fields" => "*",
            "default_sort_fields" => "mbd_usb_id",
            "valid_sort_fields" => "mbd_usb_id",
            "tuple_identifier" => null
        ),
        "monitors" => array(
            "table_name" => "monitor",
            "default_fields" => "monitor_id,company_name,model,screen_size,resolution",
            "default_sort_fields" => "monitor_id",
            "valid_sort_fields" => "monitor_id,screen_size",
            "tuple_identifier" => "CONCAT(company_name,' ',model)"
        ),
        "motherboards" => array(
            "table_name" => "motherboard",
            "default_fields" => "motherboard_id,company_name,chipset_model,model,form_factor,color",
            "default_sort_fields" => "motherboard_id",
            "valid_sort_fields" => "motherboard_id,chipset_model,form_factor,no_of_memory_slots,max_memory,no_of_expansion_slots",
            "tuple_identifier" => "CONCAT(company_name,' ',model)",
            "additional_queries" => array(
                "supported_sockets" => "SELECT mbd_socket_name FROM mbd_sockets_support WHERE mbd_model=\"%s\";",
                "supported_usb_vers" => "SELECT usb_ver FROM mbd_usb_support WHERE mbd_model=\"%s\";"
            )
        ),
        "motherboard_form_factors" => array(
            "table_name" => "mbd_form_factor",
            "default_fields" => "*",
            "default_sort_fields" => null,
            "valid_sort_fields" => null,
            "tuple_identifier" => null
        ),
        "mouse_pads" => array(
            "table_name" => "mouse_pad",
            "default_fields" => "mouse_pad_id,company_name,model,dimensions",
            "default_sort_fields" => "mouse_pad_id",
            "valid_sort_fields" => "mouse_pad_id",
            "tuple_identifier" => "CONCAT(company_name,' ',model)"
        ),
        "operating_systems" => array(
            "table_name" => "operating_system",
            "default_fields" => "*",
            "default_sort_fields" => "operating_system_id",
            "valid_sort_fields" => "operating_system_id,type,bit",
            "tuple_identifier" => "CONCAT(name,' ',version,' ',bit,'-bit ',IFNULL(edition,''))"
        ),
        "optical_drives" => array(
            "table_name" => "optical_drive",
            "default_fields" => "optical_drive_id,company_name,model,type,connector",
            "default_sort_fields" => "optical_drive_id",
            "valid_sort_fields" => "optical_drive_id,type",
            "tuple_identifier" => "CONCAT(company_name,' ',model)"
        ),
        "partners" => array(
            "table_name" => "partner",
            "default_fields" => "*",
            "default_sort_fields" => "name",
            "valid_sort_fields" => "name",
            "tuple_identifier" => null
        ),
        "psus" => array(
            "table_name" => "power_supply_unit",
            "default_fields" => "power_supply_unit_id,company_name,model,form_factor,fan_size",
            "default_sort_fields" => "power_supply_unit_id",
            "valid_sort_fields" => "power_supply_unit_id,form_factor,fan_size",
            "tuple_identifier" => "CONCAT(company_name,' ',model)"
        ),
        "prices" => array(
            "table_name" => "prices",
            "default_fields" => "*",
            "default_sort_fields" => "price_id",
            "valid_sort_fields" => "price_id,price",
            "tuple_identifier" => null
        ),
        "printers" => array(
            "table_name" => "printer",
            "default_fields" => "printer_id,company_name,model,method,speed,functions",
            "default_sort_fields" => "printer_id",
            "valid_sort_fields" => "printer_id,speed",
            "tuple_identifier" => "CONCAT(company_name,' ',model,' ',functions,' ',IFNULL(method,''),'%')"
        ),
        "products_data" => array(
            "table_name" => "products_data",
            "default_fields" => "*",
            "default_sort_fields" => "product_data_id",
            "valid_sort_fields" => "product_data_id,rating,got_time,last_modified",
            "tuple_identifier" => null
        ),
        "psu_form_factors" => array(
            "table_name" => "psu_form_factor",
            "default_fields" => "*",
            "default_sort_fields" => null,
            "valid_sort_fields" => null,
            "tuple_identifier" => null
        ),
        "rams" => array(
            "table_name" => "ram",
            "default_fields" => "ram_id,company_name,model,size,type,kit_config",
            "default_sort_fields" => "ram_id",
            "valid_sort_fields" => "ram_id,speed,size",
            "tuple_identifier" => "CONCAT('%',model,'%',kit_config,'%')"
        ),
        "ram_speeds" => array(
            "table_name" => "ram_speed",
            "default_fields" => "*",
            "default_sort_fields" => null,
            "valid_sort_fields" => null,
            "tuple_identifier" => null
        ),
        "storages" => array(
            "table_name" => "secondary_storage",
            "default_fields" => "storage_id,company_name,model,type,capacity",
            "default_sort_fields" => "storage_id",
            "valid_sort_fields" => "storage_id,capacity",
            "tuple_identifier" => "CONCAT(model,' ',capacity,' ',IFNULL(form_factor,''))"
        ),
        "sockets" => array(
            "table_name" => "sockets",
            "default_fields" => "*",
            "default_sort_fields" =>null,
            "valid_sort_fields" => null,
            "tuple_identifier" => null
        ),
        "sound_cards" => array(
            "table_name" => "sound_card",
            "default_fields" => "sound_card_id,company_name,model,type,interface",
            "default_sort_fields" => "sound_card_id",
            "valid_sort_fields" => "sound_card_id,type",
            "tuple_identifier" => "CONCAT(company_name,' ',model)"
        ),
        "speakers" => array(
            "table_name" => "speaker",
            "default_fields" => "speaker_id,company_name,model,configuration",
            "default_sort_fields" => "speaker_id",
            "valid_sort_fields" => "speaker_id,configuration",
            "tuple_identifier" => "CONCAT(company_name,' ',model)"
        ),
        "templates" => array(
            "table_name" => "templates",
            "default_fields" => "*",
            "default_sort_fields" => "template_id",
            "valid_sort_fields" => "template_id,price",
            "tuple_identifier" => null,
            /*"additional_queries" => array(
                "configuration_details" => "SELECT * FROM configuration WHERE configuration_id=\"%s\";"
            )*/
        ),
        "ups" => array(
            "table_name" => "uninterruptible_power_supply",
            "default_fields" => "ups_id,company_name,model,battery",
            "default_sort_fields" => "ups_id",
            "valid_sort_fields" => "ups_id,capacity",
            "tuple_identifier" => "CONCAT(company_name,' ',model)"
        ),
        "webcams" => array(
            "table_name" => "webcam",
            "default_fields" => "webcam_id,company_name,model,resolution",
            "default_sort_fields" => "webcam_id",
            "valid_sort_fields" => "webcam_id",
            "tuple_identifier" => "CONCAT(company_name,' ',model)"
        ),
        "wifi_adapters" => array(
            "table_name" => "wifi_adapter",
            "default_fields" => "wifi_adapter_id,company_name,model,type",
            "default_sort_fields" => "wifi_adapter_id",
            "valid_sort_fields" => "wifi_adapter_id,type",
            "tuple_identifier" => "CONCAT(company_name,' ',model)"
        ),
        "endpoints" => array(
            "table_name" => "component_endpoint",
            "default_fields" => "*",
            "default_sort_fields" => "component_name",
            "valid_sort_fields" => "component_name"
        )
    );

    /**
     * @var array Associative array containing valid organizational table names with default_fields, default_sort_fields,
     * and the permitted valid sort fields.
     */
    const ORG_TABLES = array(
        "admins" => array(
            "table_name" => "admin",
            "default_fields" => "admin_id,username",
            "default_sort_fields" => "admin_id",
        ),
        "technicians" => array(
            "table_name" => "technician",
            "default_fields" => "technician_id,first_name,last_name,area,landline_no,mobile_no_1",
            "valid_fields" => "technician_id,name,addr1,addr2,area,zipcode,landline_no,mobile_no_1,mobile_no_2,email_id,services,description",
            "signup_fields" => "technician_id,name,username,password,addr1,addr2,area,zipcode,landline_no,mobile_no_1,mobile_no_2,email_id,services,description",
            "default_sort_fields" => "technician_id",
            "valid_sort_fields" => "technician_id,area",
        ),
        "org_technicians" => array(
            "table_name" => "org_technician",
            "default_fields" => "*",
            "default_sort_fields" => "pair_id",
            "valid_sort_fields" => null,
        ),
        "organizations" => array(
            "table_name" => "organization",
            "default_fields" => "organization_id,name,addr1,addr2,area,landline_no,mobile_no_1",
            "valid_fields" => "organization_id,name,type,addr1,addr2,area,zipcode,landline_no,mobile_no_1,mobile_no_2,email_id,services,description",
            "signup_fields" => "organization_id,name,username,password,type,addr1,addr2,area,zipcode,landline_no,mobile_no_1,mobile_no_2,email_id,services,description",
            "default_sort_fields" => "organization_id",
            "valid_sort_fields" => "organization_id,area",
        ),
        "new_arrival_requests" => array(
            "table_name" => "new_arrival_request",
            "default_fields" => "*",
            "default_sort_fields" => "arrival_request_id",
            "valid_sort_fields" => "arrival_request_id,arrival_time",
        ),
        "stocks" => array(
            "table_name" => "stocks",
            "default_fields" => "org_username,product_name,quantity,price",
            "default_sort_fields" => "stock_id",
            "valid_sort_fields" => "stock_id,product_name,got_time",
        )
    );


}