<?php

/** Defines namespace for PCPartsController class */
namespace PCMaker\Controllers;

/** Alias for the Request class */
use Slim\Http\Request as Request;

/** Alias for the Response class */
use Slim\Http\Response as Response;

/** Alias for the ContainerInterface */
use Interop\Container\ContainerInterface as ContainerInterface;

/** Alias for the FeedbackMessage class */
use PCMaker\Helpers\FeedbackMessage;

/** Alias for the DataSourceFactory class */
use PCMaker\Components\SourceFactory;

/** Alias for the Constants class */
use PCMaker\Helpers\Constants;

/** Alias for all the custom exceptions */
use PCMaker\Exceptions\{
    DeletionFailed, EmptyResultSet, InsertionFailed, InvalidArgument, InvalidFieldsParameter, InvalidOrder, InvalidRequest, InvalidResponseGroup, InvalidSortParameter, ResponseGroupNotSupported, UpdateFailed
};

/** Alias for the Utils class */
use PCMaker\Helpers\Utils;

/** Brings PDOException to current namespace */
use PDOException;


/**
 * Class PCPartsController
 * A Controller for handling of requests for Hardware and stuff
 * @package PCMaker\Controllers
 */
class PCPartsController {

    /** @var ContainerInterface Stores the container */
    protected $container;


    /**
     * PCPartsController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }


    /**
     * Performs an insertion operation and returns the feedback message
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestToInsertItem(Request $request, Response $response, $args) {

        // Create a new DataSource object
        $component = SourceFactory::getPCPartsInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets the result of insertion
            $result = $component->insertItem($request->getParsedBody());

            /** @noinspection PhpUndefinedMethodInspection */
            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidArgument | InsertionFailed | PDOException $exception) {
            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Returns single object with the ID, from the requested data source
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestForSingleItem(Request $request, Response $response, $args) {

        // Gets the parameters from the URI
        $params = $request->getQueryParams();

        // Declare empty variables to store the arguments
        $fields = array(); $response_group = "info";

        // Check if the 'fields' argument is present or not
        // If present create an array from the csv line
        if (isset($params['fields'])) {
            $fields = $params['fields'];
            $fields = explode(',', $fields);
        }

        // Check if the 'response_group' argument is present or not
        // If so store it in $response_group
        if (isset($params['response_group'])) {
            $response_group = $params['response_group'];
        }

        // Create a new DataSource object
        $component = SourceFactory::getPCPartsInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets an array of result rows
            $result = $component->getSingleItem($args["id"], $fields, $response_group);

            // Sets the current_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_NUMERIC_CHECK|JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidArgument | InvalidRequest | InvalidFieldsParameter | InvalidResponseGroup | ResponseGroupNotSupported | EmptyResultSet | PDOException $exception) {
            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Returns multiple objects from the requested data source
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestForMultipleItems(Request $request, Response $response, $args) {

        // Gets the parameters from the URI
        $params = $request->getQueryParams();

        try {
            // Checks whether the arguments passed in request are valid or not
            // If not throws an InvalidArgument exception
            if (!Utils::validArgsChecker(array_keys($params), Constants::valid_args["GET"])) {
                throw new InvalidArgument();
            }

            // Stores the current query parameters
            $new_query = array();

            // Declare empty variables to store the arguments
            $fields = array(); $sort_by = array(); $response_group = "info"; $per_page = Constants::PER_PAGE; $from = 0; $order = "ASC";

            // Check if the 'fields' argument is present or not
            // If present create an array from the csv line
            if (isset($params['fields'])) {
                $fields = $params['fields'];
                $new_query["fields"] = $fields;
                $fields = explode(',', $fields);
            }

            // Check if the 'sort_by' argument is present or not
            // If present create an array from the csv line
            if (isset($params['from'])) {
                $from = $params['from'];
            }

            // Check if the 'sort_by' argument is present or not
            // If present create an array from the csv line
            if (isset($params['sort_by'])) {
                $sort_by = $params['sort_by'];
                $new_query['sort_by'] = $sort_by;
                $sort_by = explode(',', $sort_by);
            }

            // Check if the 'per_page' argument is present or not
            // If so store it in $per_page
            if (isset($params['per_page'])) {
                $per_page = (int)$params['per_page'];
                $new_query["per_page"] = $per_page;
            }

            // Check if the 'response_group' argument is present or not
            // If so store it in $response_group
            if (isset($params['response_group'])) {
                $response_group = $params['response_group'];
                $new_query["response_group"] = $response_group;
            }

            // Check if the 'order' argument is present or not
            // If so store it in $order
            if (isset($params['order'])) {
                $order = $params['order'];
                $new_query["order"] = $order;
            }

            // Gets and updates the value of from argument for the next page url
            $new_query["from"] = $from + $per_page;

            // Performs the query with provided parameters
            // Gets the current request URL
            $current_url = $request->getUri();

            // Create a new DataSource object
            $component = SourceFactory::getPCPartsInstance($args["table_name"]);

            // Gets an array of result rows
            $result = $component->getMultipleItems($fields, $sort_by, $order, $from, $per_page, $response_group);

            // Sets the current_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setCurrentLink($current_url);

            // Sets the next_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setNextPage($current_url->withQuery(http_build_query($new_query)));

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidArgument | InvalidRequest | InvalidFieldsParameter | InvalidSortParameter | InvalidResponseGroup | InvalidOrder | ResponseGroupNotSupported | EmptyResultSet | PDOException $exception) {
            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Performs an update operation and returns the feedback message
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestToUpdateItem(Request $request, Response $response, $args) {

        // Create a new DataSource object
        $component = SourceFactory::getPCPartsInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets the result of update
            $result = $component->updateItem($args["id"], $request->getParsedBody());

            // Sets the current_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_NUMERIC_CHECK|JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidArgument | UpdateFailed | PDOException $exception) {
            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Performs an delete operation and returns the feedback message
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestToDeleteItem(Request $request, Response $response, $args) {

        // Create a new DataSource object
        $component = SourceFactory::getPCPartsInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets the result of deletion
            $result = $component->deleteItem($args["id"]);

            // Sets the current_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidArgument | DeletionFailed | PDOException $exception) {
            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }

}