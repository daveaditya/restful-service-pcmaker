<?php

/** Defines namespace for AccountController class */
namespace PCMaker\Controllers;

/** Alias for the Request class */
use PCMaker\Helpers\Constants;
use PCMaker\Helpers\Utils;
use Slim\Http\Request as Request;

/** Alias for the Response class */
use Slim\Http\Response as Response;

/** Alias for the ContainerInterface */
use Interop\Container\ContainerInterface as ContainerInterface;

/** Alias for the FeedbackMessage class */
use PCMaker\Helpers\FeedbackMessage;

/** Alias for the DataSourceFactory class */
use PCMaker\Components\SourceFactory;

/** Alias for all the custom exceptions */
use PCMaker\Exceptions\{
    AccountDeletionFailed, DeletionFailed, EmptyResultSet, InsertionFailed, InvalidArgument, InvalidFieldsParameter, InvalidID, InvalidRequest,
    InvalidResponseGroup, InvalidSortParameter, InvalidUsername, NothingToDelete, ResponseGroupNotSupported, UpdateFailed
};

/** Brings PDOException to current namespace */
use PDOException;


/**
 * Class AccountController
 * A Controller for handling of requests for Organizations and Technicians
 * @package PCMaker\Controllers
 */
class AccountController {

    /** @var ContainerInterface Stores the container */
    protected $container;


    /**
     * AccountController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }


    /**
     * Returns single object with the ID, from the requested data source
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestForAccountDetails(Request $request, Response $response, $args) {

        // Gets the parameters from the URI
        $params = $request->getQueryParams();

        // Declare empty variables to store the arguments
        $fields = array();

        // Check if the 'fields' argument is present or not
        // If present create an array from the csv line
        if (isset($params['fields'])) {
            $fields = $params['fields'];
            $fields = explode(',', $fields);
        }

        // Create a new AccountSource object
        $component = SourceFactory::getAccountInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets an array of result rows
            $result = $component->getDetails($args["username"], $fields);

            // Sets the current_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidArgument | InvalidRequest | InvalidFieldsParameter | InvalidResponseGroup | ResponseGroupNotSupported | EmptyResultSet | PDOException $exception) {
            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Returns multiple objects from the requested data source
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestForMultipleAccounts(Request $request, Response $response, $args) {

        // Gets the parameters from the URI
        $params = $request->getQueryParams();

        try {
            // Checks whether the arguments passed in request are valid or not
            // If not throws an InvalidArgument exception
            if (!Utils::validArgsChecker(array_keys($params), Constants::valid_args["GET"])) {
                throw new InvalidArgument();
            }

            // Stores the current query parameters
            $new_query = array();

            // Declare empty variables to store the arguments
            $fields = array(); $sort_by = array(); $per_page = Constants::PER_PAGE; $from = 0;

            // Check if the 'fields' argument is present or not
            // If present create an array from the csv line
            if (isset($params['fields'])) {
                $fields = $params['fields'];
                $new_query["fields"] = $fields;
                $fields = explode(',', $fields);
            }

            // Check if the 'sort_by' argument is present or not
            // If present create an array from the csv line
            if (isset($params['from'])) {
                $from = $params['from'];
            }

            // Check if the 'sort_by' argument is present or not
            // If present create an array from the csv line
            if (isset($params['sort_by'])) {
                $sort_by = $params['sort_by'];
                $new_query['sort_by'] = $sort_by;
                $sort_by = explode(',', $sort_by);
            }

            // Check if the 'per_page' argument is present or not
            // If so store it in $per_page
            if (isset($params['per_page'])) {
                $per_page = (int)$params['per_page'];
            }

            // Check if the 'response_group' argument is present or not
            // If so store it in $response_group
            if (isset($params['response_group'])) {
                $response_group = $params['response_group'];
                $new_query["response_group"] = $response_group;
            }

            // Gets the value of per_page argument to use in next page url
            $new_query["per_page"] = $per_page;

            // Gets and updates the value of from argument for the next page url
            $new_query["from"] = $from + $per_page;

            // Performs the query with provided parameters
            // Gets the current request URL
            $current_url = $request->getUri();

            // Create a new DataSource object
            $component = SourceFactory::getAccountInstance($args["table_name"]);

            // Gets an array of result rows
            $result = $component->getMultipleAccounts($fields, $sort_by, $from, $per_page);

            // Sets the current_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setCurrentLink($current_url);

            // Sets the next_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setNextPage($current_url->withQuery(http_build_query($new_query)));

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidArgument | InvalidRequest | InvalidFieldsParameter | InvalidSortParameter | InvalidResponseGroup | ResponseGroupNotSupported | EmptyResultSet | PDOException $exception) {
            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Performs an update operation on account and returns the feedback message
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestToUpdateAccountDetails(Request $request, Response $response, $args) {

        // Create a new DataSource object
        $component = SourceFactory::getAccountInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets the result of update
            $result = $component->updateAccountDetails($args["username"], $request->getParsedBody());

            // Sets the current_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidArgument | UpdateFailed | PDOException $exception) {
            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Performs an delete operation and returns the feedback message
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestToDeleteAccount(Request $request, Response $response, $args) {

        // Create a new DataSource object
        $component = SourceFactory::getAccountInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets the result of deletion
            $result = $component->deleteAccount($request->getParsedBody());

            // Sets the current_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_NUMERIC_CHECK|JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidArgument | InvalidUsername | AccountDeletionFailed | PDOException $exception) {

            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Connects a technician to the organization
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestToAddTechnician(Request $request, Response $response, $args) {

        // Create a new DataSource object
        $component = SourceFactory::getAccountInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets the result of insertion
            $result = $component->connectNewTechnician($args["username"], $request->getParsedBody());

            /** @noinspection PhpUndefinedMethodInspection */
            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_NUMERIC_CHECK|JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidArgument | InsertionFailed | InvalidUsername | PDOException $exception) {
            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Returns a single technician based on id and organization's username
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestForSingleRegisteredTechnician(Request $request, Response $response, $args) {

        // Create a new AccountSource object
        $component = SourceFactory::getAccountInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets an array of result rows
            $result = $component->getSingleConnectedTechnician($args["id"]);

            // Sets the current_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidID | EmptyResultSet | PDOException $exception) {
            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Returns the technicians registered with given organization
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestForRegisteredTechnicians(Request $request, Response $response, $args) {

        // Declare empty variables to store the arguments
        $per_page = Constants::PER_PAGE; $from = 0;

        // Check if the 'sort_by' argument is present or not
        // If present create an array from the csv line
        if (isset($params['from'])) {
            $from = $params['from'];
        }

        // Check if the 'per_page' argument is present or not
        // If so store it in $per_page
        if (isset($params['per_page'])) {
            $per_page = (int)$params['per_page'];
        }

        // Gets the value of per_page argument to use in next page url
        $new_query["per_page"] = $per_page;

        // Gets and updates the value of from argument for the next page url
        $new_query["from"] = $from + $per_page;

        // Create a new AccountSource object
        $component = SourceFactory::getAccountInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets an array of result rows
            $result = $component->getConnectedTechnicians($args["username"], $from, $per_page);

            // Sets the current_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidUsername | EmptyResultSet | PDOException $exception) {
            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Deletes the specified username from the organization
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestToRemoveTechnician(Request $request, Response $response, $args) {

        // Create a new DataSource object
        $component = SourceFactory::getAccountInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets the result of deletion
            $result = $component->removeTechnician($args["org_username"], $args["tech_username"]);

            // Sets the current_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (DeletionFailed | NothingToDelete | PDOException $exception) {

            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Adds a stock for the given organization
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestToAddStock(Request $request, Response $response, $args) {

        // Create a new DataSource object
        $component = SourceFactory::getAccountInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets the result of insertion
            $result = $component->addNewStockInfo($args["username"], $request->getParsedBody());

            /** @noinspection PhpUndefinedMethodInspection */
            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidArgument | InsertionFailed | InvalidUsername | PDOException $exception) {
            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Returns the stock information for the given organization or technician, and id
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestToGetStock(Request $request, Response $response, $args) {

        // Create a new AccountSource object
        $component = SourceFactory::getAccountInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets an array of result rows
            $result = $component->getStockInfo($args["username"], $args["id"]);

            // Sets the current_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidID | EmptyResultSet | PDOException $exception) {
            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Returns multiple stock tuple
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     * @throws InvalidArgument
     */
    public function requestToGetMultipleStock(Request $request, Response $response, $args) {

        // Gets the parameters from the URI
        $params = $request->getQueryParams();

        try {
            // Checks whether the arguments passed in request are valid or not
            // If not throws an InvalidArgument exception
            if (!Utils::validArgsChecker(array_keys($params), Constants::valid_args["GET"])) {
                throw new InvalidArgument();
            }

            // Stores the current query parameters
            $new_query = array();

            // Declare empty variables to store the arguments
            $per_page = Constants::PER_PAGE; $from = 0;

            // Check if the 'sort_by' argument is present or not
            // If present create an array from the csv line
            if (isset($params['from'])) {
                $from = $params['from'];
            }

            // Check if the 'per_page' argument is present or not
            // If so store it in $per_page
            if (isset($params['per_page'])) {
                $per_page = (int)$params['per_page'];
            }

            // Gets the value of per_page argument to use in next page url
            $new_query["per_page"] = $per_page;

            // Gets and updates the value of from argument for the next page url
            $new_query["from"] = $from + $per_page;

            // Performs the query with provided parameters
            // Gets the current request URL
            $current_url = $request->getUri();

            // Create a new DataSource object
            $component = SourceFactory::getAccountInstance($args["table_name"]);

            // Gets an array of result rows
            $result = $component->getMultipleStockInfo($args["username"], $from, $per_page);

            // Sets the current_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setCurrentLink($current_url);

            // Sets the next_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setNextPage($current_url->withQuery(http_build_query($new_query)));

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (EmptyResultSet | PDOException $exception) {
            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Updates the stock information
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestToUpdateStock(Request $request, Response $response, $args) {

        // Create a new DataSource object
        $component = SourceFactory::getAccountInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets the result of update
            $result = $component->updateStockInfo($args["username"], $request->getParsedBody());

            // Sets the current_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidArgument | InvalidID | UpdateFailed | PDOException $exception) {
            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Delete the stock tuple with given id
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestToDeleteStock(Request $request, Response $response, $args) {

        // Create a new DataSource object
        $component = SourceFactory::getAccountInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets the result of deletion
            $result = $component->deleteStockInfo($args["id"]);

            // Sets the current_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidID | DeletionFailed | PDOException $exception) {

            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Adds a new arrival item
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestToAddNewArrival(Request $request, Response $response, $args) {

        // Create a new DataSource object
        $component = SourceFactory::getAccountInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets the result of insertion
            $result = $component->addNewArrival($args["username"], $request->getParsedBody());

            /** @noinspection PhpUndefinedMethodInspection */
            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidArgument | InsertionFailed | InvalidUsername | PDOException $exception) {
            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Returns multiple new arrival request tuple
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestToGetNewArrivals(Request $request, Response $response, $args) {

        // Create a new AccountSource object
        $component = SourceFactory::getAccountInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets an array of result rows
            $result = $component->getNewArrivals($args["username"]);

            // Sets the current_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (EmptyResultSet | PDOException $exception) {
            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Delete the new arrival tuple with given id
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestToDeleteNewArrival(Request $request, Response $response, $args) {

        // Create a new DataSource object
        $component = SourceFactory::getAccountInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets the result of deletion
            $result = $component->deleteNewArrival($args["id"]);

            // Sets the current_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidID | DeletionFailed | PDOException $exception) {

            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }

}