<?php

/** Defines namespace for AuthController class */
namespace PCMaker\Controllers;

/** Alias for the Request class */
use Slim\Http\Request as Request;

/** Alias for the Response class */
use Slim\Http\Response as Response;

/** Alias for the ContainerInterface */
use Interop\Container\ContainerInterface as ContainerInterface;

/** Alias for the FeedbackMessage class */
use PCMaker\Helpers\FeedbackMessage;

/** Alias for the SourceFactory class */
use PCMaker\Components\SourceFactory;

/** Alias for all the custom exceptions */
use PCMaker\Exceptions\{
    InsertionFailed, InvalidArgument, InvalidPassword, InvalidUsername
};

/** Brings PDOException to current namespace */
use PDOException;


/**
 * Class AuthController
 * A Controller for handling of requests for Organizations and Technicians
 * @package PCMaker\Controllers
 */
class AuthController {

    /** @var ContainerInterface Stores the container */
    protected $container;


    /**
     * AccountController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }


    /**
     * Performs the login check for the entered Data Source and given username and password
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestToLogin(Request $request, Response $response, $args) {

        // Create a new AccountSource object
        $component = SourceFactory::getAccountInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets the result of insertion
            $result = $component->login($request->getParsedBody());

            // Sets the current_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidPassword | InvalidUsername | PDOException $exception) {
            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Performs the signup operation for the entered Data Source using given data
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestToSignUp(Request $request, Response $response, $args) {

        // Create a new DataSource object
        $component = SourceFactory::getAccountInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets the result of insertion of new data
            $result = $component->signup($request->getParsedBody());

            // Sets the current_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidArgument | InsertionFailed | PDOException $exception) {
            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 200, JSON_UNESCAPED_SLASHES);
        }

    }


    /**
     * Performs the operation of changing password for a given account
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function requestToResetPassword(Request $request, Response $response, $args) {

        var_dump($args);

        // Create a new DataSource object
        $component = SourceFactory::getAccountInstance($args["table_name"]);

        // Performs the query with provided parameters
        try {
            // Gets the result of insertion of new data
            $result = $component->resetPassword($request->getParsedBody());

            // Sets the current_url of the FeedbackMessage
            /** @noinspection PhpUndefinedMethodInspection */
            $result["feedback"]->setCurrentLink($request->getUri());

            // Converts the result into JSON and returns a new Response object
            return $response->withJson($result, 200, JSON_UNESCAPED_SLASHES);

            // Handles RuntimeException that might occur creating the JSON response
        } catch (InvalidArgument | InsertionFailed | PDOException $exception) {
            // An empty array to hold the response
            $result = array();

            // Add the FeedbackMessage to the response
            $result["feedback"] = new FeedbackMessage();

            // Sets response to FAILURE
            $result["feedback"]->setResponse(FeedbackMessage::FAILURE);

            // Sets the error_type to the name of class the exception belong to
            $result["feedback"]->setErrorType((new \ReflectionClass($exception))->getShortName());

            // Sets the current_url of the FeedbackMessage
            $result["feedback"]->setCurrentLink($request->getUri());

            // Sets the error_message
            $result["feedback"]->setErrorMessage($exception->getMessage());

            // Since no rows will be returned, set num_of_rows to zero
            $result["feedback"]->setNumOfRows(0);

            // return the response
            return $response->withJson($result, 404, JSON_UNESCAPED_SLASHES);
        }

    }

}